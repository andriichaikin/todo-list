import React, {Component} from 'react';
import {Redirect} from "react-router-dom";

import './title.css';

import ProfileMenu from "../../components/user/profile/menu/profile-menu";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlus} from "@fortawesome/free-solid-svg-icons";

import {connect} from "react-redux";
import {logout} from '../../store/actions/auth-action';
import UploadAvatarContainer from "../user/profile/upload-avatar/upload-avatar-container";
import ErrorBoundary from "../../hoc/error/error-boundary";
import {baseUrl} from "../../utils/API";
import Avatar from "../../components/avatar/avatar";

class Title extends Component {

    constructor(props) {
        super(props);

        //todo preload imgs
        this.state = {
            isOpenedUserMenu: false,
            isOpenedUploadAvatarModal: false,
            redirectToCreate: false,
            redirectToUserProfileSettings: false,
        };
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.refreshRedirectBtn();
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.onMouseDown, false);
    }

    refreshRedirectBtn() {
        if (this.state.redirectToCreate) this.setState({redirectToCreate: false});
        if (this.state.redirectToUserProfileSettings) this.setState({redirectToUserProfileSettings: false});

    }

    toggleUserMenu = (e, toggleAction) => {
        const isOpenedUserMenu = toggleAction === undefined ? !this.state.isOpenedUserMenu : toggleAction;

        isOpenedUserMenu ?
            document.addEventListener('mousedown', this.onMouseDown, false) :
            document.removeEventListener('mousedown', this.onMouseDown, false);

        this.setState({isOpenedUserMenu});
    };

    uploadAvatarModal = () => {
        this.toggleUserMenu(null, false);
        this.setState({isOpenedUploadAvatarModal: true})
    };

    closeUploadAvatarModal = () => this.setState({isOpenedUploadAvatarModal: false});

    onMouseDown = (e) => {
        let isClickedOutsideOfElem = !this.node.contains(e.target);
        if (isClickedOutsideOfElem) this.toggleUserMenu(null, false);

        return false;
    };

    redirectToCreate = () => this.setState({redirectToCreate: true});

    logout = () => this.props.logout();

    handleError = () => {
        /*todo change cat img*/
        return (
            <div className='todos-error--wrapper'>
                     <span className='todos-error__text'>
                        Sorry, an exception occurred while rendering title :(
                    </span>
                <img className='todos-error__img'
                     src={`${baseUrl}/images/cat_crying.png`}
                     alt="cat_crying"/>
            </div>
        )
    };

    goToUserProfileSettings  = () => {
       this.setState({redirectToUserProfileSettings: true});
    };

    render() {
        const Error = this.handleError;
        const {avatar} = this.props.user;

        return (
            <ErrorBoundary customRender={<Error/>}>
                <>
                    {this.state.isOpenedUploadAvatarModal ? <div className='upload-avatar__full-dark-bg'></div> : null}
                    {this.state.redirectToCreate ? <Redirect to='/todo/create'/> : null}
                    {this.state.redirectToUserProfileSettings? <Redirect to='/user/profile/settings'/> : null}

                    <div ref={node => this.node = node} className="title">
                        <div className='title__caption--create-todo-wrapper'
                             onClick={this.redirectToCreate}>
                            <FontAwesomeIcon icon={faPlus} className='title__caption--create-todo-plus-icon'/>
                        </div>
                        <div className='title__caption'>
                            <h2 className='title__caption--name'>Todo list</h2>
                        </div>
                        <div className='title__user-avatar title__user-avatar--small'
                             onClick={this.toggleUserMenu}>
                            <Avatar avatar={avatar} size={35}/>
                            {
                                this.state.isOpenedUserMenu ?
                                    <ProfileMenu user={this.props.user}
                                                 uploadAvatarModal={this.uploadAvatarModal}
                                                 toggleUserMenu={this.toggleUserMenu}
                                                 userProfileSettings={this.goToUserProfileSettings}
                                                 logout={this.logout}
                                                 onClick={this.toggleUserMenu}/> : null
                            }
                        </div>
                        {
                            this.state.isOpenedUploadAvatarModal ?
                                <UploadAvatarContainer closeModal={this.closeUploadAvatarModal}/> : null
                        }
                    </div>
                </>
            </ErrorBoundary>
        )
    }
}

const mapStateToProps = state => ({
    user: state.userReducer,
    auth: state.authReducer,
});

const mapDispatchToProps = dispatch => ({
    logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Title);
