import React from 'react';

import './button.css';

const Button = ({name, onClick, cName = ''}) => {
    const classes = 'btn ' + cName;

    return <button className={classes} onClick={onClick}>{name}</button>
};

export default Button;
