import {LOGIN_USER, LOGOUT_USER, LOGIN_ERROR, HANDLE_LOGIN_INPUT_CHANGES} from "../constants";

const authReducer = (state = initialState, action) => {
    const {id, isAuthenticated, hasError} = action;

    switch (action.type) {
        case LOGIN_USER: {
            state = {id, isAuthenticated};
            return state;
        }
        case LOGOUT_USER: {
            state = {id, isAuthenticated};
            return state;
        }
        case LOGIN_ERROR: {
            state = {...state, id, password: '', isAuthenticated, hasError};
            return state;
        }
        case HANDLE_LOGIN_INPUT_CHANGES: {
            const {property, value} = action;
            state = {...state, [property]: value};
            return state;
        }
        default:
            return state;
    }
};

export default authReducer;

export const initialState = {
    id: undefined,
    username: 'jonny',
    password: 'password',
    isAuthenticated: false,
    hasError: false,
};
