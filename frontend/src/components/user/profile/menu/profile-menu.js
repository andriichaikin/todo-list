import React from 'react';

import './profile-menu.css';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCameraRetro} from "@fortawesome/free-solid-svg-icons";
import {NavLink, Redirect} from "react-router-dom";
import {FormattedMessage} from "react-intl";
import Avatar from "../../../avatar/avatar";

const ProfileMenu = (props) => {

    const {user, uploadAvatarModal, toggleUserMenu, logout, userProfileSettings} = props;
    const {name, surname, avatar} = user;

    return (
        <nav className='profile-menu'>
                <ul className='profile-menu__navs'>
                    <li className='profile-menu__nav'>
                        <div className='title__user-avatar profile-menu__nav--img-wrapper'
                             onClick={uploadAvatarModal}>
                            <Avatar avatar={avatar} size={96}/>
                            <FontAwesomeIcon icon={faCameraRetro} className='profile-menu__nav--camera'/>
                        </div>
                        <div className='profile-menu__nav--info'>
                            <p>{name} {surname}</p>

                            <NavLink to='/user/profile'
                                     className='profile-menu__nav--profile'
                                     onClick={toggleUserMenu}>
                                <FormattedMessage id='profile.menu.item.profile' defaultMessage='Profile'/>
                            </NavLink>
                        </div>
                    </li>
                    <li onClick={userProfileSettings}>
                        <FormattedMessage id='profile.menu.item.settings' defaultMessage='Settings'/>
                    </li>
                    <li onClick={logout}>
                        <FormattedMessage id='profile.menu.item.logout' defaultMessage='Log out'/>
                    </li>
                </ul>
            </nav>
    )
};

export default ProfileMenu;
