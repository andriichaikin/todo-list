import React, {Component} from 'react';
import {connect} from "react-redux";

import './circular-spinner.css';
import spinnerReducer from "../../../store/reducers/spinner-reducer";

class CircularSpinner extends Component {


    getSpinner = () => {
        const {isLoading} = this.props.spinner;

        return isLoading ?
            <div className="circular-spinner">
                <i className="loading"></i>
            </div> : null
    };

    render() {
        const Loading = this.getSpinner;

        return (
            <>
                <Loading/>
                {this.props.children}
            </>
        )
    }
}

const mapStateToProps = state => ({
    spinner: state.spinnerReducer,
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(CircularSpinner);
