import {
    FETCH_USER_AVATAR_FAILED,
    FETCH_USER_AVATAR_SUCCESS,
    FETCH_USER_INFO_FAILED,
    FETCH_USER_INFO_SUCCESS, FETCH_USER_SETTINGS_FAILED, FETCH_USER_SETTINGS_SUCCESS,
    SAVE_USER_FAILED,
    SAVE_USER_SUCCESS, TOGGLE_USER_LANG_FAILED, TOGGLE_USER_LANG_SUCCESS,
    UPDATE_USER_PROPERTY,
    USER_AVATAR_UPLOAD_FAILED,
} from '../../constants';

const userReducer = (state = initialState, action) => {

    switch (action.type) {
        case FETCH_USER_INFO_SUCCESS: {
            const {name, surname, email, phone} = action;
            state = {...state, name, surname, email, phone};
            return state;
        }
        case FETCH_USER_INFO_FAILED: {
            return state;
        }
        case FETCH_USER_SETTINGS_SUCCESS: {
            state = {
                ...state,
                settings: {
                    ...state.settings,
                    lang: action.lang
                }
            };
            return state;
        }
        case FETCH_USER_SETTINGS_FAILED: {
            return state;
        }
        case SAVE_USER_SUCCESS: {
            return state;
        }
        case SAVE_USER_FAILED: {
            return state;
        }
        case UPDATE_USER_PROPERTY: {
            state = {
                ...state,
                [action.key]: action.value
            };
            return state;
        }
        case USER_AVATAR_UPLOAD_FAILED: {
            state = {
                ...state,
                isLoading: false
            };
            return state;
        }
        case FETCH_USER_AVATAR_SUCCESS: {
            state = {
                ...state,
                avatar: {
                    source: action.source,
                    type: action.type,
                }
            };
            return state;
        }
        case FETCH_USER_AVATAR_FAILED: {
            state = {
                ...state,
                avatar: {
                    source: undefined,
                    type: undefined,
                }
            };
            return state;
        }
        case TOGGLE_USER_LANG_SUCCESS: {
            state= {
                ...state,
                settings: {
                    ...state.settings,
                    lang: action.lang,
                }
            };
            return state;
        }
        case TOGGLE_USER_LANG_FAILED: {
            return state;
        }
        default: {
            return state;
        }
    }
};

export default userReducer;

const initialState = {
    name: '',
    surname: '',
    email: '',
    phone: '',
    settings: {
        lang: '',
    },
    avatar: {
        source: undefined,
        type: undefined,
    }
};
