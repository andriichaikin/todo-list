package ua.todo.dao;

import javax.transaction.Transactional;

import ua.todo.pojo.Consumer;

import java.util.Optional;

@Transactional
public interface ConsumerDao {

    void save(Consumer user);

    void update(Consumer user);

    Optional<Consumer> findById(Long id);

    Optional<Consumer> findByLogin(String username);
}
