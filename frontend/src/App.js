import React, {Component} from 'react';

import './reset.css';
import './index.css';
import './App.css';
import Title from './containers/title/title';
import {connect} from "react-redux";
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import CircularSpinner from './containers/spinner/circular/circular-spinner';
import TodoList from "./containers/todo/list/todo-list";
import CreateTodo from "./containers/todo/create/create-todo";
import EditTodo from "./containers/todo/edit/edit-todo";
import ProfileInfo from "./containers/user/profile/edit/user-profile";
import LoginPage from "./containers/login/login-page";
import RegistrationPage from './containers/registration/registration';
import {decodeLocaleStorageToken, isTokenValid, removeToken} from "./auth/auth-utils";
import {setAuthorized} from "./store/actions/auth-action";
import {LOGIN_USER} from "./store/constants";
import {IntlProvider} from "react-intl";

import messages_en from './translations/en.json';
import messages_ua from './translations/ua.json';
import {fetchUser, fetchUserSettings} from "./store/actions/user/user-action";
import ProfileSettings from "./containers/user/profile/settings/profile-settings";
import {DEFAULT_SETTINGS} from "./utils/constants";
import {PrivateRoute} from "./components/auth/private-router";
import {fetchUserAvatar} from "./store/actions/user/user-avatar-action";

const messages = {
    'en': messages_en,
    'ua': messages_ua,
};

class App extends Component {


    componentWillMount() {
        const {isAuthenticated} = this.props.auth;

        if (!isAuthenticated) {
            const decoded = decodeLocaleStorageToken();

            if (decoded && isTokenValid(decoded)) {
                const {id} = decoded;
                const {setAuthorized} = this.props;

                setAuthorized(LOGIN_USER, id, true);

            } else {
                removeToken();
            }
        }
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {auth} = this.props;
        if(auth.isAuthenticated && !prevProps.auth.isAuthenticated) {
            const {id} = auth;
            const {fetchUser, fetchUserSettings, fetchUserAvatar} = this.props;

            fetchUser(id);
            fetchUserSettings(id);
            fetchUserAvatar(id);
        }
    }

    render() {
        const {isAuthenticated} = this.props.auth;
        const {user} = this.props;
        const {lang} = user && user.settings && user.settings.lang ? user.settings : DEFAULT_SETTINGS;

        return (
            <IntlProvider locale='en' messages={messages[`${lang}`]}>
                <div className='app__body'>
                    <CircularSpinner>
                        <BrowserRouter>
                            {isAuthenticated ? <Title/> : <Redirect to='/login'/>}
                            <Switch>
                                <Route exact path='/' component={TodoList}/>
                                <Route path='/login' component={LoginPage}/>
                                <Route path='/sign-up' component={RegistrationPage}/>
                                <PrivateRoute path='/todo/create' component={CreateTodo}
                                              isAuth={isAuthenticated}/>
                                <PrivateRoute path='/todo/edit/:id' component={EditTodo}
                                              isAuth={isAuthenticated}/>
                                <PrivateRoute exact path='/user/profile' component={ProfileInfo}
                                              isAuth={isAuthenticated}/>
                                <PrivateRoute exact path='/user/profile/settings' component={ProfileSettings}
                                              isAuth={isAuthenticated}/>
                            </Switch>
                        </BrowserRouter>
                    </CircularSpinner>
                </div>
            </IntlProvider>
        );
    };
}

const mapStateToProps = state => ({
    auth: state.authReducer,
    user: state.userReducer,
});

const mapDispatchToProps = dispatch => ({
    setAuthorized: (type, id, isAuthorized) => dispatch(setAuthorized(type, id, isAuthorized)),
    fetchUser: (id) => dispatch(fetchUser(id)),
    fetchUserSettings: (id) => dispatch(fetchUserSettings(id)),
    fetchUserAvatar: (id) => dispatch(fetchUserAvatar(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
