package ua.todo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import ua.todo.dto.ConsumerInfoDTO;
import ua.todo.pojo.Consumer;
import ua.todo.service.ConsumerService;
import ua.todo.service.FileService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Optional;

import static java.lang.String.format;
import static org.apache.logging.log4j.util.Strings.isBlank;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static ua.todo.dto.ConsumerDTOFactory.createInfo;
import static ua.todo.dto.ConsumerDTOFactory.createSettins;

@CrossOrigin
@RestController
@RequestMapping("/api/user")
public class ConsumerController {

    @Autowired
    private ConsumerService consumerService;

    @Autowired
    private FileService fileService;

    @Value("${consumer.dir.path.avatar}")
    private String pathAvatar;

    @RequestMapping(value = "/{id}/get", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getUserInfo(@PathVariable Long id) {

        Optional<Consumer> user = consumerService.findById(id);
        if (user.isPresent())
            return ResponseEntity.ok(createInfo(user.get()));

        return ResponseEntity.status(CONFLICT).body(format("Consumer %s not found", id));
    }

    @RequestMapping(value = "/{id}/profile/settings")
    public ResponseEntity getUserSettings(@PathVariable Long id) {

        Consumer user = consumerService.findById(id)
                .orElseThrow(() ->
                        new ResponseStatusException(BAD_REQUEST, format("Can not find user by id %s", id)));

        return ResponseEntity.ok(createSettins(user.getSettings()));
    }

    @RequestMapping(value = "/{id}/update", method = POST, consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity updateUserInfo(@PathVariable Long id,
                                         @RequestBody ConsumerInfoDTO info) {

        Consumer founded = consumerService.findById(id)
                .orElseThrow(() -> new ResponseStatusException(CONFLICT, format("Can not find user by id %s", id)));

        consumerService.updateUserInfo(founded, info);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/{id}/avatar/upload", method = POST,
            consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity uploadAvatar(@PathVariable Long id,
                                       @RequestParam("file") MultipartFile file) throws IOException {

        String path = format(pathAvatar, id);
        if (!fileService.dirsExist(path)) fileService.createDirs(path);

        File create = new File(path + file.getOriginalFilename());
        FileOutputStream outputStream = new FileOutputStream(create);
        outputStream.write(file.getBytes());
        outputStream.close();

        consumerService.setAvatar(id, file.getOriginalFilename());

        return ResponseEntity.ok().build();
    }


    @RequestMapping(value = "/{id}/avatar/get", method = GET)
    @ResponseBody
    public ResponseEntity getUserAvatar(@PathVariable Long id) throws IOException {

        Consumer consumer = consumerService.findById(id)
                .orElseThrow(() ->
                        new ResponseStatusException(BAD_REQUEST, format("Can not find user by id %s", id)));
        if (consumer.getSettings() == null || isBlank(consumer.getSettings().getAvatarPath()))
            return ResponseEntity.noContent().build();

        String path = format(pathAvatar, id) + consumer.getSettings().getAvatarPath();

        String encoded = fileService.encodeFile(path);
        if (isBlank(encoded)) return ResponseEntity.noContent().build();

        return ResponseEntity.ok(encoded);
    }

    @RequestMapping(value = "/{id}/language/set", method = GET)
    public ResponseEntity setUserLanguage(@PathVariable Long id,
                                          @RequestParam String lang) {
        Consumer consumer = consumerService.findById(id)
                .orElseThrow(() ->
                        new ResponseStatusException(BAD_REQUEST, format("Can not find user by id %s", id)));
        consumerService.setLanguage(consumer, lang);

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/registration", method = POST)
    public ResponseEntity register(@RequestBody Consumer consumer) {
        validateConsumer(consumer);

        String encode = new BCryptPasswordEncoder().encode(consumer.getPassword());
        consumer.setPassword(encode);

        consumerService.save(consumer);
        return ResponseEntity.ok().build();
    }

    private void validateConsumer(@RequestBody Consumer consumer) {
        if(consumer == null) throw new ResponseStatusException(BAD_REQUEST, "Consumer is null");
        if(isBlank(consumer.getLogin())) throw new ResponseStatusException(BAD_REQUEST, "Consumer login is empty");
        if(isBlank(consumer.getPassword())) throw new ResponseStatusException(BAD_REQUEST, "Consumer password is empty");
        if(isBlank(consumer.getName())) throw new ResponseStatusException(BAD_REQUEST, "Consumer name is empty");
    }
}
