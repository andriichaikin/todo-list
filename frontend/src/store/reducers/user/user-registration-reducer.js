import {HANDLE_REGISTRATION_INPUT_CHANGES, REGISTRATION_FAILED, REGISTRATION_SUCCESS} from "../../constants";

const userRegistrationReducer = (state = initialState, action) => {
    switch (action.type) {
        case HANDLE_REGISTRATION_INPUT_CHANGES: {
            state = {
                ...state,
                user: {
                    ...state.user,
                    [action.key]: action.value
                },
            };
            debugger
            return state;
        }
        case REGISTRATION_SUCCESS: {
            state = {
                ...state,
                isRegistered: true,
                hasError: false,
            };
            return state;
        }
        case REGISTRATION_FAILED: {
            state = {
                ...state,
                isRegistered: false,
                hasError: true,
            };
            return state;
        }
        default:
            return state;
    }
};

export default userRegistrationReducer;

const initialState = {
    user: {
        login: 'asdfasdf',
        password: 'asdfasdaf',
        name: 'asdfasdf',
        surname: 'asdf',
        email: 'asdf',
        phone: 'asdf',
    },
    isRegistered: undefined
};
