package ua.todo.dto;

public class ConsumerSettingsDTO {

    private String avatar;
    private String language;

    public String getAvatar() {
        return avatar;
    }

    public ConsumerSettingsDTO setAvatar(String avatar) {
        this.avatar = avatar;
        return this;
    }

    public String getLanguage() {
        return language;
    }

    public ConsumerSettingsDTO setLanguage(String language) {
        this.language = language;
        return this;
    }
}
