INSERT INTO consumer_settings (avatar_path, lang) values ('', 'UKRAINIAN');

INSERT INTO consumer (login, password, name, surname, email, phone, fk_user_settings) VALUES ('jonny', '$2a$04$I/yzgsa8LvHm6Lvb3kqKeuGPD4dYvpH4FD6Pkb9ZJNAGrRJ7YvHxS', 'John', 'Doe', 'john.doe@mail.com', '+1234123412', 1);

INSERT INTO task (name, description, is_completed, is_removed, fk_consumer) values ('task_1', 'description_1', false, false, 1);

INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('MONDAY', true, 1);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('TUESDAY', false, 1);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('WEDNESDAY', false, 1);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('THURSDAY', false, 1);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('FRIDAY', false, 1);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('SATURDAY', false, 1);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('SUNDAY', false, 1);


INSERT INTO task (name, description, is_completed, is_removed, fk_consumer) values ('task_2', 'description_2', false, false, 1);

INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('MONDAY', true, 2);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('TUESDAY', false, 2);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('WEDNESDAY', false, 2);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('THURSDAY', false, 2);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('FRIDAY', false, 2);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('SATURDAY', false, 2);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('SUNDAY', false, 2);


INSERT INTO task (name, description, is_completed, is_removed, fk_consumer) values ('task_3', 'description_3', false, false, 1);

INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('MONDAY', true, 3);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('TUESDAY', false, 3);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('WEDNESDAY', false, 3);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('THURSDAY', false, 3);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('FRIDAY', false, 3);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('SATURDAY', false, 3);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('SUNDAY', false, 3);


INSERT INTO task (name, description, is_completed, is_removed, fk_consumer) values ('task_4', 'description_4', false, false, 1);

INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('MONDAY', true, 4);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('TUESDAY', false, 4);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('WEDNESDAY', false, 4);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('THURSDAY', false, 4);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('FRIDAY', false, 4);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('SATURDAY', false, 4);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('SUNDAY', false, 4);


INSERT INTO task (name, description, is_completed, is_removed, fk_consumer) values ('task_5', 'description_5', false, false, 1);

INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('MONDAY', true, 5);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('TUESDAY', false, 5);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('WEDNESDAY', false, 5);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('THURSDAY', false, 5);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('FRIDAY', false, 5);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('SATURDAY', false, 5);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('SUNDAY', false, 5);


INSERT INTO task (name, description, is_completed, is_removed, fk_consumer) values ('task_6', 'description_6', false, false, 1);

INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('MONDAY', true, 6);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('TUESDAY', true, 6);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('WEDNESDAY', true, 6);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('THURSDAY', true, 6);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('FRIDAY', true, 6);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('SATURDAY', false, 6);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('SUNDAY', false, 6);


INSERT INTO task (name, description, is_completed, is_removed, fk_consumer) values ('task_7', 'description_7', false, false, 1);

INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('MONDAY', true, 7);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('TUESDAY', true, 7);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('WEDNESDAY', true, 7);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('THURSDAY', true, 7);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('FRIDAY', true, 7);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('SATURDAY', true, 7);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('SUNDAY', true, 7);


INSERT INTO task (name, description, is_completed, is_removed, fk_consumer) values ('task_8', 'description_8', true, false, 1);

INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('MONDAY', true, 8);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('TUESDAY', true, 8);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('WEDNESDAY', true, 8);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('THURSDAY', true, 8);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('FRIDAY', true, 8);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('SATURDAY', true, 8);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('SUNDAY', true, 8);


INSERT INTO task (name, description, is_completed, is_removed, fk_consumer) values ('task_9', 'description_9', true, false, 1);

INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('MONDAY', false, 9);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('TUESDAY', false, 9);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('WEDNESDAY', false, 9);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('THURSDAY', false, 9);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('FRIDAY', false, 9);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('SATURDAY', false, 9);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('SUNDAY', false, 9);


INSERT INTO task (name, description, is_completed, is_removed, fk_consumer) values ('task_10', 'description_10', true, false, 1);

INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('MONDAY', false, 10);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('TUESDAY', false, 10);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('WEDNESDAY', false, 10);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('THURSDAY', false, 10);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('FRIDAY', false, 10);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('SATURDAY', false, 10);
INSERT INTO weekday (day, is_enabled, fk_task) VALUES ('SUNDAY', false, 10);