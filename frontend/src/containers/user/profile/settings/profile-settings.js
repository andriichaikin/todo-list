import React, {Component} from 'react';

import './profile-settings.css';
import {connect} from "react-redux";
import {DEFAULT_SETTINGS} from "../../../../utils/constants";
import {setUserLanguage} from "../../../../store/actions/user/user-action";
import {FormattedMessage} from "react-intl";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimesCircle} from "@fortawesome/free-solid-svg-icons";
import {Redirect} from "react-router-dom";

class ProfileSettings extends Component {

    state = {
        redirectToMain: false,
    };

    componentWillUnmount() {
        this.setState({redirectToMain: false});
    }

    toggleLanguage = () => {
        const {id} = this.props.auth;
        const switchedLang = this.getSwitchedLang();

        const {setLanguage} = this.props;
        setLanguage(id, switchedLang)
    };

    activeSwitchKnob = () => {
        const {user} = this.props;
        const {lang} = user && user.settings && user.settings.lang ? user.settings : '';

        let activeClasses = 'profile-settings__active_';
        switch (lang) {
            case 'ua': {
                activeClasses += 'ua';
                break;
            }
            case 'en': {
                activeClasses += 'en';
                break;
            }
            default:
                // activeClasses += 'en';
                break;
        }

        return <div className={`profile-settings__switch-knob ${activeClasses}`}></div>

    };

    getSwitchedLang = () => {
        const {user} = this.props;
        const {lang} = user && user.settings && user.settings.lang ? user.settings : DEFAULT_SETTINGS;

        return lang === 'en' ? 'ua' : 'en';
    };

    close = () => {
        this.setState({redirectToMain: true});
    };

    render() {
        const ActiveKnob = this.activeSwitchKnob;
        const {redirectToMain} = this.state;
        return (
            redirectToMain ? <Redirect to='/'/> :
                <div className='profile-settings'>
                    <div className='profile-settings__wrapper'>
                        <FontAwesomeIcon icon={faTimesCircle}
                                         size='2x'
                                         className='profile-settings__close-icon'
                                         onClick={this.close}/>
                        <div className='profile-settings__lang'>
                            <FormattedMessage id='profile.user.language.switch' defaultMessage='Select language'/>
                            <div className='profile-settings__switch-frame'
                                 onClick={this.toggleLanguage}>
                                <ActiveKnob/>
                            </div>
                        </div>
                    </div>
                </div>
        )
    }
}

const mapStateToProps = state => ({
    auth: state.authReducer,
    user: state.userReducer,
});

const mapDispatchToProps = dispatch => ({
    setLanguage: (id, lang) => dispatch(setUserLanguage(id, lang)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileSettings);
