import {RESET_LOCAL_TODO_DATA, UPDATE_TODO_DAY, UPDATE_TODO_PROPERTY,} from "../../constants";

export const updateTodoProperty = (key, value) => {
    const action = {
        type: UPDATE_TODO_PROPERTY,
        key,
        value,
    };
    return action;
};

export const updateTodoDay = (key, checked) => {
    const action = {
        type: UPDATE_TODO_DAY,
        key,
        checked,
    };

    return action;
};

export const resetLocalData = () => {
    const action = {
        type: RESET_LOCAL_TODO_DATA,
    };
    return action;
};
