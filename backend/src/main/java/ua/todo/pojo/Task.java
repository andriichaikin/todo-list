package ua.todo.pojo;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

import static java.lang.String.format;
import static javax.persistence.CascadeType.ALL;

@Entity(name = "Task")
@Table(name = "task")
public class Task extends BaseEntity<Task> {

    public static final String TASK_NAME = "name";
    public static final String TASK_DESCRIPTION = "description";
    public static final String IS_COMPLETED = "is_completed";
    public static final String IS_REMOVED = "is_removed";

    @Column(name = TASK_NAME)
    @NotNull
    private String name;

    @Column(name = TASK_DESCRIPTION)
    private String description;

    @Column(name = IS_COMPLETED)
    private boolean isCompleted;

    @Column(name = IS_REMOVED)
    private boolean isRemoved;

    @OneToMany(
            fetch = FetchType.EAGER,
            cascade = ALL,
            mappedBy = "task",
            orphanRemoval = true)
    @JsonManagedReference
    private List<Weekday> days = new LinkedList<>();

    @ManyToOne(optional = false)
    @JoinColumn(name = "fk_consumer")
    @JsonBackReference
    private Consumer consumer;

    public void setDayEnabled(String name, boolean enable) {
        days.stream()
                .filter(d -> d.getDay().name().equalsIgnoreCase(name))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(format("Can not find day by name %s", name)))
                .setEnabled(enable);
    }

    public boolean isDayEnabled(String name) {
        return days.stream()
                .filter(d -> d.getDay().name().equalsIgnoreCase(name))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(format("Can not find day by name %s", name)))
                .isEnabled();
    }

    public String getName() {
        return name;
    }

    public Task setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Task setDescription(String description) {
        this.description = description;
        return this;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public Task setCompleted(boolean completed) {
        isCompleted = completed;
        return this;
    }

    public Task setRemoved(boolean removed) {
        isRemoved = removed;
        return this;
    }

    public boolean isRemoved() {
        return this.isRemoved;
    }

    public List<Weekday> getDays() {
        return days;
    }

    public void setDays(List<Weekday> days) {
        this.days = days;
    }

    public Consumer getConsumer() {
        return consumer;
    }

    public Task setConsumer(Consumer consumer) {
        this.consumer = consumer;
        return this;
    }

    @Override
    public String toString() {
        return super.toString() +
                "Task{" +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", isCompleted=" + isCompleted +
                ", isRemoved=" + isRemoved +
                ", days=" + days +
                '}';
    }

}
