import {
    FETCH_USER_INFO_FAILED,
    FETCH_USER_INFO_SUCCESS, FETCH_USER_SETTINGS_FAILED, FETCH_USER_SETTINGS_SUCCESS,
    SAVE_USER_FAILED,
    SAVE_USER_SUCCESS, TOGGLE_USER_LANG_FAILED, TOGGLE_USER_LANG_SUCCESS,
    UPDATE_USER_PROPERTY,
} from "../../constants";
import API from "../../../utils/API";
import {toggleSpinner} from "../spinner-action";

export const fetchUser = (id) => (dispatch, getState) => {
    dispatch(toggleSpinner(true));

    API.get(`/user/${id}/get`)
        .then(response => {
            const {name, surname, email, phone} = response.data;
            dispatch(__fetchUserSuccess(name, surname, email, phone));
        })
        .catch(err => __fetchUserFailed())
        .finally(() => dispatch(toggleSpinner(false)));

};

const __fetchUserSuccess = (name, surname, email, phone) => {
    const action = {
        type: FETCH_USER_INFO_SUCCESS,
        name, surname, email, phone
    };

    return action;
};

const __fetchUserFailed = () => {
    const action = {
        type: FETCH_USER_INFO_FAILED,
        fetchUserFailed: true,
    };
    return action;
};

export const fetchUserSettings = id => dispatch => {
    dispatch(toggleSpinner(true));

    API.get(`/user/${id}/profile/settings`)
        .then(response => {
            const {language} = response.data;
            dispatch(__fetchUserSettingsSuccess(language));
        })
        .catch(() => __fetchUserSettingsFailed())
        .finally(() => dispatch(toggleSpinner(false)));
};

const __fetchUserSettingsSuccess = (lang) => {
    const action = {
        type: FETCH_USER_SETTINGS_SUCCESS,
        lang,
    };
    return action;
};

const __fetchUserSettingsFailed = () => {
    const action = {
        type: FETCH_USER_SETTINGS_FAILED,
        lang: 'en',
    };
    return action;
};

export const saveUser = (id, user) => dispatch => {
    const {name, surname, email, phone} = user;

    dispatch(toggleSpinner(true));
    API.post(`/user/${id}/update`, {
        name, surname, email, phone
    })
        .then(() => {

            dispatch(__saveUserSuccess());

            // this.props.history.push('/');
        })
        .catch(() => __saveUserFailed())
        .finally(() => dispatch(toggleSpinner(false)));
};

const __saveUserSuccess = () => {
    const action = {
        type: SAVE_USER_SUCCESS,
        saveSuccess: true,
    };
    return action;
};

const __saveUserFailed = () => {
    const action = {
        type: SAVE_USER_FAILED,
        saveFailed: true,
    };
    return action;
};

export const updateUserProperty = (key, value) => {
    const action = {
        type: UPDATE_USER_PROPERTY,
        key,
        value,
    };
    return action;
};

export const setUserLanguage = (id, lang) => dispatch => {
    dispatch(toggleSpinner(true));
    API.get(`/user/${id}/language/set?lang=${lang}`)
        .then(res => dispatch(__toggleUserLanguageSuccess(lang)))
        .catch(() => dispatch(__toggleUserLanguageFailed()))
        .finally(() => dispatch(toggleSpinner(false)));
};

const __toggleUserLanguageSuccess = lang => {
    const action = {
        type: TOGGLE_USER_LANG_SUCCESS,
        lang,
    };
    return action;
};

const __toggleUserLanguageFailed = () => {
    const action = {
        type: TOGGLE_USER_LANG_FAILED,
    };
    return action;
};
