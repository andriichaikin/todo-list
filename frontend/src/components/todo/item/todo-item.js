import React from 'react';

import './todo-item.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCheckCircle} from "@fortawesome/free-solid-svg-icons";
import {faEdit} from "@fortawesome/free-solid-svg-icons/faEdit";


const Description = ({description, showDescription}) => {

    return showDescription ? <div className="todo-item__description">{description}</div> : null
};

const CompletedIcon = ({id, isCompleted, toggleIsCompleted}) => {
    let clazz = 'todo-item__icon';
    if (isCompleted) clazz += ' todo-item__icon--completed';
    else clazz += ' todo-item__icon--not-completed';

    return isCompleted ?
        <FontAwesomeIcon icon={faCheckCircle}
                         className={clazz}
                         onClick={toggleIsCompleted.bind(this, id)}/> :
        <div className={clazz}
             onClick={toggleIsCompleted.bind(this, id)}>&nbsp;</div>
};

const goToEdit = (props, e) => {
    e.stopPropagation();

    const {todo} = props;
    props.history.push(`/todo/edit/${todo.id}`, {todo});
};

const TodoItem = (props) => {
    const {todo, showDescription, toggleDescription, toggleIsCompleted} = props;
    const {id, name, description, completed} = todo;

    let itemNameClass = 'todo-item__name';
    if (completed) itemNameClass += ' todo-item__name--completed';

    return (
        <li className='todo-item'
            onClick={toggleDescription.bind(this, id)}>
            <CompletedIcon id={id}
                           isCompleted={completed}
                           toggleIsCompleted={toggleIsCompleted.bind(this)}/>
            <span className={itemNameClass}>{name}</span>
            <FontAwesomeIcon icon={faEdit}
                             className='todo-item__icon todo-item__icon--edit'
                             onClick={goToEdit.bind(this, props)}/>
            <Description description={description}
                         showDescription={showDescription}/>
        </li>
    )
};


export default TodoItem;
