package ua.todo.dto;

import ua.todo.pojo.Consumer;
import ua.todo.pojo.ConsumerSettings;

public class ConsumerDTOFactory {

    public static ConsumerInfoDTO createInfo(Consumer user) {
        if (user == null) throw new IllegalArgumentException("Consumer is null");

        return new ConsumerInfoDTO()
                .setName(user.getName())
                .setSurname(user.getSurname())
                .setEmail(user.getEmail())
                .setPhone(user.getPhone());
    }

    public static ConsumerSettingsDTO createSettins(ConsumerSettings settings) {
        if(settings == null) throw new IllegalArgumentException("Settings is null");

        return new ConsumerSettingsDTO()
                .setLanguage(settings.getLang().value());
    }
}
