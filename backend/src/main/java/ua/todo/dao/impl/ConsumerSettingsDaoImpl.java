package ua.todo.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.todo.dao.ConsumerSettingsDao;
import ua.todo.pojo.ConsumerSettings;

import javax.transaction.Transactional;

@Repository
@Transactional
public class ConsumerSettingsDaoImpl implements ConsumerSettingsDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void saveOrUpdate(ConsumerSettings userSettings) {
        sessionFactory.getCurrentSession().saveOrUpdate(userSettings);
    }
}
