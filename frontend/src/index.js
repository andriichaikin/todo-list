import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

import {addLocaleData} from "react-intl";
import locale_en from 'react-intl/locale-data/en';

import {Provider} from 'react-redux';
import * as serviceWorker from './serviceWorker';
import store from "./store/store";

addLocaleData(...locale_en);

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
