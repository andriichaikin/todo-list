import React, {Component} from 'react';

import './user-profile.css';

import {connect} from "react-redux";
import {saveUser, updateUserProperty} from "../../../../store/actions/user/user-action";

import TextInput from "../../../../components/common/textinput/text-input";
import Button from "../../../../components/common/button/button";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimesCircle} from "@fortawesome/free-solid-svg-icons";
import ErrorBoundary from "../../../../hoc/error/error-boundary";
import {baseUrl} from "../../../../utils/API";
import {FormattedMessage} from "react-intl";

class UserProfile extends Component {

    handleInput = (e) => {
        const {name, value} = e.target;

        this.props.updateProperty(name, value);
    };

    handleButton = (e) => {
        e.preventDefault();

        const {id} = this.props.auth;
        const {user} = this.props;


        this.props.saveUser(id, user);
    };

    redirectToMain = () => {
        this.props.history.push('/')
    };

    handleError = () => {
        /*todo change cat img*/
        return (
            <div className='todos-error--wrapper'>
                     <span className='todos-error__text'>
                        Sorry, an exception occurred while modifying user profile :(
                    </span>
                <img className='todos-error__img'
                     src={`${baseUrl}/images/cat_crying.png`}
                     alt="cat_crying"/>
            </div>
        )
    };

    render() {
        const Error = this.handleError;

        const {name, surname, email, phone} = this.props.user;

        const nameLabel = <FormattedMessage id='profile.user.label.name' defaultMessage='Name'/>;
        const surnameLabel = <FormattedMessage id='profile.user.label.surname' defaultMessage='Surname'/>;
        const phoneLabel = <FormattedMessage id='profile.user.label.phone' defaultMessage='Phone'/>;
        const emailLabel = <FormattedMessage id='profile.user.label.email' defaultMessage='E-mail'/>;
        const btnName = <FormattedMessage id='profile.user.btn.name' defaultMessage='Save'/>;

        return (
            <ErrorBoundary customRender={<Error/>}>
                <div className='profile-info--wrapper'>
                    <div className="profile-info">
                        <FontAwesomeIcon icon={faTimesCircle}
                                         size='2x'
                                         className='profile-info__close-icon'
                                         onClick={this.redirectToMain}/>
                        <ul className='profile-info__items'>
                            <li><TextInput name='name'
                                           value={name}
                                           cName='profile-info__item-input'
                                           label={nameLabel}
                                           onChange={this.handleInput}
                                           autoComplete={false}/>
                            </li>
                            <li><TextInput name='surname'
                                           value={surname}
                                           cName='profile-info__item-input'
                                           label={surnameLabel}
                                           onChange={this.handleInput}
                                           autoComplete={false}/>
                            </li>
                            <li><TextInput name='phone'
                                           value={phone}
                                           cName='profile-info__item-input'
                                           label={phoneLabel}
                                           type='phone'
                                           onChange={this.handleInput}
                                           autoComplete={false}/>
                            </li>
                            <li><TextInput name='email'
                                           value={email}
                                           cName='profile-info__item-input'
                                           label={emailLabel}
                                           type='email'
                                           onChange={this.handleInput}
                                           autoComplete={false}/>
                            </li>
                            <li>
                                <Button name={btnName} onClick={this.handleButton} cName='profile-info__btn'/>
                            </li>
                        </ul>
                    </div>
                </div>
            </ErrorBoundary>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.userReducer,
        auth: state.authReducer,
    }
};

const mapDispatchToProps = dispatch => ({
    saveUser: (id, user) => dispatch(saveUser(id, user)),
    updateProperty: (key, value) => dispatch(updateUserProperty(key, value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);
