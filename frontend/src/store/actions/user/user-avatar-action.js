import {
    FETCH_USER_AVATAR_FAILED,
    FETCH_USER_AVATAR_SUCCESS,
    USER_AVATAR_ATTACH_FILE,
    USER_AVATAR_REMOVE_ATTACHED_FILE,
    USER_AVATAR_SET_UPLOADING,
    USER_AVATAR_UPLOAD_FAILED,
    USER_AVATAR_UPLOAD_SUCCESS
} from '../../constants';
import API from "../../../utils/API";
import {toggleSpinner} from "../spinner-action";

export const uploadFileAction = (consumerId, file) => (dispatch, getState) => {
    dispatch(setIsLoading(true));

    const formData = new FormData();
    formData.append("file", file);

    dispatch(toggleSpinner(true));
    API.post(`/user/${consumerId}/avatar/upload`, formData, {
        headers: {
            'Content-Type': 'multipart/form-data',
        },
        onUploadProgress: progressEvent => {
            const percentage = parseInt(Math.round((progressEvent.loaded * 100)) / progressEvent.total);
            console.log('percentage: ' + percentage);
        }
    })
        .then(res => dispatch(uploadFileSuccess()))
        .catch(() => dispatch(uploadFileError()))
        .finally(() => {
            dispatch(toggleSpinner(false));
            // dispatch(setIsLoading(false))
        });
};

export const attachFile = (file) => {
    const action = {
        type: USER_AVATAR_ATTACH_FILE,
        file,
    };
    return action;
};

export const rmAttachedFile = () => {
    const action = {
        type: USER_AVATAR_REMOVE_ATTACHED_FILE,
        file: undefined,
    };
    return action;
};

const setIsLoading = (isLoading) => {
    const action = {
        type: USER_AVATAR_SET_UPLOADING,
        isLoading,
    };

    return action;
};

const uploadFileSuccess = () => {
    const action = {
        type: USER_AVATAR_UPLOAD_SUCCESS,
        file: undefined,
        isLoading: false,
    };
    return action;
};

const uploadFileError = () => {
    const action = {
        type: USER_AVATAR_UPLOAD_FAILED,
        isLoading: false,
    };
    return action;
};


export const fetchUserAvatar = id => dispatch => {
    API.get(`/user/${id}/avatar/get`, {
        responseType: 'arraybuffer'
    })
        .then(response => {
            const data = response.data;

            if(data) {
                const str = new Buffer(data).toString();
                dispatch(__fetchUserAvatarSuccess(str, 'image/jpg'));
            }
        })
        .catch(err => {
            dispatch(__fetchUserAvatarFailed());
        });
};

const __fetchUserAvatarSuccess = (source, mediaType) => {
    const action = {
        type: FETCH_USER_AVATAR_SUCCESS,
        source,
        mediaType,
    };
    return action;
};

const __fetchUserAvatarFailed = () => {
    const action = {
        type: FETCH_USER_AVATAR_FAILED,
    };
    return action;
};
