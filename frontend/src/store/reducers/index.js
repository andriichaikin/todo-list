import {combineReducers} from 'redux';

import userReducer from "./user/user-reducer";
import authReducer from "./auth-reducer";
import userAvatarReducer from "./user/user-avatar-reducer";
import todoListReducer from "./todo/todo-list-reducer";
import todoModifyReducer from "./todo/todo-modify-reducer";
import spinnerReducer from "./spinner-reducer";
import userRegistrationReducer from "./user/user-registration-reducer";


const rootReducer = combineReducers({
    authReducer,

    todoListReducer,
    todoModifyReducer,

    userReducer,
    userAvatarReducer,
    userRegistrationReducer,

    spinnerReducer,
});


export default rootReducer;
