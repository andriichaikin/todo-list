import React, {Component} from 'react';
import {connect} from "react-redux";
import {resetLocalData, updateTodoDay, updateTodoProperty} from "../../../store/actions/todo/todo-modify-action";
import {Redirect} from "react-router-dom";

function withStateManager(WrappedComponent) {

    class TodoStateHandler extends Component {

        state = {
            redirectToMain: false,
        };

        componentWillUnmount() {
            this.props.resetLocalData();
        }

        redirectToMain = () => {
            this.setState({redirectToMain: true});
        };

        handleInputChanges = (e) => {
            e.preventDefault();

            const {name, value} = e.target;
            const {updateTodoProperty} = this.props;

            updateTodoProperty(name, value);
        };

        handleCheckboxChanges = (e) => {
            const {name, checked} = e.target;
            const {updateDay} = this.props;

            updateDay(name, checked);
        };

        validate = () => {
            const {todo} = this.props;

            return todo.name && todo.name.length > 2;
        };

        render() {
            return this.state.redirectToMain ?
                <Redirect to='/'/> :
                <WrappedComponent todo={this.props.todo}
                                  info={this.props.info}
                                  handleInputChanges={this.handleInputChanges}
                                  handleCheckboxChanges={this.handleCheckboxChanges}
                                  redirectToMain={this.redirectToMain}
                                  validate={this.validate}
                                  {...this.props}/>
        }
    }

    const mapStateToProps = state => ({
        todo: state.todoModifyReducer.todo,
        info: state.todoModifyReducer.info,
    });

    const mapDispatchToProps = dispatch => ({
        updateTodoProperty: (key, value) => dispatch(updateTodoProperty(key, value)),
        updateDay: (name, checked) => dispatch(updateTodoDay(name, checked)),
        resetLocalData: () => dispatch(resetLocalData()),
    });

    return connect(mapStateToProps, mapDispatchToProps)(TodoStateHandler)
}

export default withStateManager;
