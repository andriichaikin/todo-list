package ua.todo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.todo.pojo.Task;
import ua.todo.pojo.CompareResult;
import ua.todo.service.TaskService;


import java.util.List;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
@RequestMapping("/api/task/")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @RequestMapping(value = "/find/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity findById(@PathVariable Long id) {

        Optional<Task> founded = taskService.findById(id);
        if (founded.isPresent()) return ResponseEntity.ok(founded.get());

        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/find/all", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    public List<Task> findAll(@RequestParam(name = "consumerId") Long consumerId,
                              @RequestParam(defaultValue = "0", required = false) int start,
                              @RequestParam(defaultValue = "10", required = false) int amount,
                              @RequestParam(
                                      name = "completedflag",
                                      defaultValue = "false",
                                      required = false) boolean completedFlag,
                              @RequestParam(
                                      name = "completed",
                                      defaultValue = "false",
                                      required = false) boolean isCompleted) {

        if (completedFlag) {
            List<Task> tasks = taskService.findAllByConsumerBatch(consumerId, start, amount, isCompleted);
            return tasks;
        }

        List<Task> tasks = taskService.findAllByConsumerBatch(consumerId, start, amount);
        return tasks;
    }

    @RequestMapping(value = "/find/all/day", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    public List<Task> findAllByDay(@RequestParam(name = "consumerId") Long consumerId,
                                   @RequestParam(defaultValue = "0", required = false) int start,
                                   @RequestParam(defaultValue = "10", required = false) int amount,
                                   @RequestParam(name = "val") String day) {

        return taskService.findAllByConsumerAndDay(consumerId, day, start, amount);
    }

    @RequestMapping(value = "/find/all/count", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Long> findAllCount(@RequestParam(name = "consumerId") Long consumerId,
                                             @RequestParam
                                                     (name = "completedflag", defaultValue = "false", required = false)
                                                     boolean completedFlag,
                                             @RequestParam(
                                                     name = "completed",
                                                     defaultValue = "false",
                                                     required = false) boolean isCompleted) {

        if (completedFlag) return ResponseEntity.ok(taskService.findAllByConsumerCount(consumerId, isCompleted));

        return ResponseEntity.ok(taskService.findAllByConsumerCount(consumerId));
    }

    @RequestMapping(value = "/all/search", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity findAllByNameOrDescription(@RequestParam(name = "consumerId") Long consumerId,
                                                     @RequestParam String val) {

        List<Task> found = taskService.findAllByConsumerAndNameOrDescription(consumerId, val);

        return ResponseEntity.ok(found);
    }

    @RequestMapping(value = "/create", method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity createTask(@RequestParam(name = "consumerId") Long consumerId,
                                     @RequestBody Task task) {

        CompareResult cr = taskService.validateTask(task);
        if (cr.isInvalid()) return ResponseEntity.status(HttpStatus.CONFLICT).body(cr);

        taskService.addTask(consumerId, task);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @RequestMapping(value = "/update", method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity updateTask(@RequestBody Task task) {

        CompareResult cr = taskService.validateTask(task);
        if (cr.isInvalid()) return ResponseEntity.status(HttpStatus.CONFLICT).body(cr);

        taskService.update(task);

        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{id}/update/completed/toggle", method = GET)
    public ResponseEntity toggleCompleted(@PathVariable Long id) {

        taskService.toggleCompleted(id);

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/{id}/remove", method = GET)
    public ResponseEntity setRemoved(@PathVariable Long id) {

        taskService.setRemoved(id);

        return ResponseEntity.ok().build();
    }
}
