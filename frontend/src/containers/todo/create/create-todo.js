import React, {Component} from 'react';

import './create-todo.css'
import TodoForm from '../../../components/todo/form/todo-form';

import {connect} from 'react-redux';

import {saveTodo} from '../../../store/actions/todo/todo-create-action';
import withStateManager from "../../../hoc/todo/modify/modify-todo";
import ErrorBoundary from "../../../hoc/error/error-boundary";
import {baseUrl} from "../../../utils/API";

class CreateTodo extends Component {

    saveOrUpdateTodo = () => {
        const {name, description, days} = this.props.todo;

        const isValid = this.props.validate();
        if (!isValid) {

            // this.setState({errorMsg: 'Name must be at least 3 characters long'});
            return;
        }

        const {id} = this.props.auth;
        this.props.saveTodo(id, name, description, days);
    };

    handleError = () => {
        /*todo change cat img*/
        return (
            <div className='todos-error--wrapper'>
                     <span className='todos-error__text'>
                        Sorry, an exception occurred while creating task :(
                    </span>
                <img className='todos-error__img'
                     src={`${baseUrl}/images/cat_crying.png`}
                     alt="cat_crying"/>
            </div>
        )
    };

    render() {
        const Error = this.handleError;
        const {handleInputChanges, handleCheckboxChanges, redirectToMain} = this.props;

        const {todo, info} = this.props;
        const {name, description, days} = todo;

        return (
            <ErrorBoundary customRender={<Error/>}>
                <div className="create-todo">
                    <TodoForm name={name}
                              description={description}
                              days={days}
                              actionInfo={info}
                              handleInputChanges={handleInputChanges}
                              handleCheckboxChanges={handleCheckboxChanges}
                              closeWithoutSaving={redirectToMain}
                              saveOrUpdateTodo={this.saveOrUpdateTodo}/>
                </div>
            </ErrorBoundary>
        )
    }
}

const mapStateToProps = state => ({auth: state.authReducer});

const mapDispatchToProps = dispatch => ({
    saveTodo: (consumerId, name, description, days) => dispatch(saveTodo(consumerId, name, description, days)),
});

const Wrapped = withStateManager(CreateTodo);

export default connect(mapStateToProps, mapDispatchToProps)(Wrapped);
