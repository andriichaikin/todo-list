package ua.todo.dao.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ua.todo.Application;
import ua.todo.config.HibernateConfiguration;
import ua.todo.dao.ConsumerDao;
import ua.todo.dao.TaskDao;
import ua.todo.pojo.Consumer;
import ua.todo.pojo.Task;
import ua.todo.pojo.Weekday;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static ua.todo.pojo.Weekday.Day.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class, HibernateConfiguration.class})
@Transactional
public class TaskDaoTest {

    private static String NAME_ = "Name_";
    private static String DESCRIPTION_ = "description_";

    @Autowired
    private TaskDao dao;

    @Autowired
    private ConsumerDao consumerDao;

    private Consumer currentConsumer;
    private Consumer someConsumerWithTasks;
    private Task task;

    @Before
    public void setUp() {
        task = new Task().setName(NAME_).setDescription(DESCRIPTION_);

        currentConsumer = new Consumer()
                .setLogin("testLogin")
                .setPassword("PasswordTest")
                .setName("testName")
                .setSurname("testSurname");
        consumerDao.save(currentConsumer);

        someConsumerWithTasks = new Consumer().setLogin("anotherConsumer").setPassword("WithAnotherTasks");
        consumerDao.save(someConsumerWithTasks);
    }

    @Test
    public void findByIdTest() {
        dao.save(task.setConsumer(currentConsumer));

        Optional<Task> found = dao.findById(task.getId());
        assertThat(found.isPresent()).isTrue();
    }

    @Test
    public void getBatchSizeAmount() {
        List<Task> batch0 = dao.findAllByConsumerBatch(currentConsumer.getId(), 0, 10);
        assertThat(batch0).isEmpty();

        prepareAndSaveTasks(7, true, currentConsumer);
        prepareAndSaveTasks(9, true, someConsumerWithTasks);

        List<Task> batch1 = dao.findAllByConsumerBatch(currentConsumer.getId(), 0, 10);
        assertThat(batch1.size()).isEqualTo(7);

        prepareAndSaveTasks(7, false, currentConsumer);
        prepareAndSaveTasks(9, false, someConsumerWithTasks);

        List<Task> batch2 = dao.findAllByConsumerBatch(currentConsumer.getId(), 0, 10);
        assertThat(batch2.size()).isEqualTo(10);

        List<Task> batch3 = dao.findAllByConsumerBatch(currentConsumer.getId(), 10, 10);
        assertThat(batch3.size()).isEqualTo(4);
    }

    @Test
    public void testBatchStartAmountCompletedTrueTest() {
        prepareAndSaveTasks(4, true, currentConsumer);
        prepareAndSaveTasks(4, false, currentConsumer);
        prepareAndSaveTasks(9, true, someConsumerWithTasks);
        prepareAndSaveTasks(9, false, someConsumerWithTasks);


        List<Task> batch0 = dao.findAllByConsumerBatch(currentConsumer.getId(), 0, 10, true);
        assertThat(batch0.size()).isEqualTo(4);

        List<Task> batch1 = dao.findAllByConsumerBatch(currentConsumer.getId(), 0, 10, false);
        assertThat(batch1.size()).isEqualTo(4);

        prepareAndSaveTasks(15, true, currentConsumer);
        prepareAndSaveTasks(15, false, currentConsumer);
        prepareAndSaveTasks(4, true, someConsumerWithTasks);
        prepareAndSaveTasks(4, false, someConsumerWithTasks);

        List<Task> batch2 = dao.findAllByConsumerBatch(currentConsumer.getId(), 0, 10, true);
        assertThat(batch2.size()).isEqualTo(10);

        List<Task> batch3 = dao.findAllByConsumerBatch(currentConsumer.getId(), 0, 10, false);
        assertThat(batch3.size()).isEqualTo(10);

        List<Task> batch4 = dao.findAllByConsumerBatch(currentConsumer.getId(), 10, 10, true);
        assertThat(batch4.size()).isEqualTo(9);

        List<Task> batch5 = dao.findAllByConsumerBatch(currentConsumer.getId(), 10, 10, false);
        assertThat(batch5.size()).isEqualTo(9);
    }

    @Test
    public void saveTest() {
        Long id = dao.save(task.setConsumer(currentConsumer));

        assertThat(dao.findById(id).isPresent()).isTrue();
    }

    @Test
    public void updateTest() {
        dao.save(task.setConsumer(currentConsumer));

        String updatedVal = "changed";
        dao.update(task.setDescription(updatedVal));

        Optional<Task> updated = dao.findById(task.getId());
        assertThat(updated.get().getDescription()).isEqualTo(updatedVal);
    }

    @Test
    public void findAllCount() {
        assertThat(dao.findAllByConsumerCount(currentConsumer.getId())).isEqualTo(0L);

        prepareAndSaveTasks(9, false, currentConsumer);
        prepareAndSaveTasks(9, false, someConsumerWithTasks);
        assertThat(dao.findAllByConsumerCount(this.currentConsumer.getId())).isEqualTo(9L);

        prepareAndSaveTasks(22, true, currentConsumer);
        prepareAndSaveTasks(3, false, someConsumerWithTasks);
        assertThat(dao.findAllByConsumerCount(this.currentConsumer.getId())).isEqualTo(31L);
    }

    @Test
    public void findAllCountCompletedTrueTest() {
        assertThat(dao.findAllByConsumerCount(currentConsumer.getId(), true)).isEqualTo(0L);

        prepareAndSaveTasks(7, false, currentConsumer);
        prepareAndSaveTasks(5, true, currentConsumer);
        prepareAndSaveTasks(4, true, someConsumerWithTasks);
        prepareAndSaveTasks(4, false, someConsumerWithTasks);

        assertThat(dao.findAllByConsumerCount(currentConsumer.getId(), true)).isEqualTo(5L);

        prepareAndSaveTasks(3, false, currentConsumer);
        prepareAndSaveTasks(9, true, currentConsumer);
        prepareAndSaveTasks(4, true, someConsumerWithTasks);
        prepareAndSaveTasks(4, false, someConsumerWithTasks);

        assertThat(dao.findAllByConsumerCount(currentConsumer.getId(), true)).isEqualTo(14L);
    }

    @Test
    public void findAllCountCompletedFalseTest() {
        assertThat(dao.findAllByConsumerCount(currentConsumer.getId(), false)).isEqualTo(0L);

        prepareAndSaveTasks(10, false, currentConsumer);
        prepareAndSaveTasks(9, true, currentConsumer);
        prepareAndSaveTasks(4, true, someConsumerWithTasks);
        prepareAndSaveTasks(4, false, someConsumerWithTasks);

        assertThat(dao.findAllByConsumerCount(currentConsumer.getId(), false)).isEqualTo(10L);

        prepareAndSaveTasks(8, false, currentConsumer);
        prepareAndSaveTasks(9, true, currentConsumer);
        prepareAndSaveTasks(4, true, someConsumerWithTasks);
        prepareAndSaveTasks(4, false, someConsumerWithTasks);

        assertThat(dao.findAllByConsumerCount(currentConsumer.getId(), true)).isEqualTo(18L);
    }

    @Test
    public void findAllByNameAndDescriptionCountTest() {
        assertThat(dao.findAllByConsumerAndNameOrDescription(currentConsumer.getId(), NAME_).isEmpty()).isTrue();

        prepareAndSaveTasks(8, false, "Pre" + NAME_, "abcabc", currentConsumer);
        prepareAndSaveTasks(6, false, "notMatch", "diff", currentConsumer);
        prepareAndSaveTasks(8, false, "Pre" + NAME_, "abcabc", someConsumerWithTasks);
        prepareAndSaveTasks(6, false, "notMatch", "diff", someConsumerWithTasks);

        assertThat(dao.findAllByConsumerAndNameOrDescriptionCount(currentConsumer.getId(), NAME_)).isEqualTo(8L);

        prepareAndSaveTasks(15, true, "notMatch", DESCRIPTION_ + "Suf", currentConsumer);
        prepareAndSaveTasks(1, true, "some text", "notMatch", currentConsumer);
        prepareAndSaveTasks(15, true, "notMatch", DESCRIPTION_ + "Suf", someConsumerWithTasks);
        prepareAndSaveTasks(1, true, "some text", "notMatch", someConsumerWithTasks);

        assertThat(dao.findAllByConsumerAndNameOrDescriptionCount(currentConsumer.getId(), DESCRIPTION_)).isEqualTo(15L);
    }

    @Test
    public void findAllByNameTest() {
        assertThat(dao.findAllByConsumerAndNameOrDescription(currentConsumer.getId(), NAME_).isEmpty()).isTrue();

        prepareAndSaveTasks(7, false, "Pre" + NAME_, DESCRIPTION_, currentConsumer);
        prepareAndSaveTasks(5, false, "notMatch", DESCRIPTION_, currentConsumer);
        prepareAndSaveTasks(7, false, "Pre" + NAME_, DESCRIPTION_, someConsumerWithTasks);
        prepareAndSaveTasks(5, false, "notMatch", DESCRIPTION_, someConsumerWithTasks);
        assertThat(dao.findAllByConsumerAndNameOrDescription(currentConsumer.getId(), NAME_).size()).isEqualTo(7L);

        prepareAndSaveTasks(17, true, NAME_ + "SUF", DESCRIPTION_, currentConsumer);
        prepareAndSaveTasks(3, true, "notMatch", DESCRIPTION_, currentConsumer);
        prepareAndSaveTasks(17, true, NAME_ + "SUF", DESCRIPTION_, someConsumerWithTasks);
        prepareAndSaveTasks(3, true, "notMatch", DESCRIPTION_, someConsumerWithTasks);
        assertThat(dao.findAllByConsumerAndNameOrDescription(currentConsumer.getId(), NAME_).size()).isEqualTo(24L);
    }

    @Test
    public void findAllByDescriptionTest() {
        assertThat(dao.findAllByConsumerAndNameOrDescription(currentConsumer.getId(), NAME_).isEmpty()).isTrue();

        prepareAndSaveTasks(8, false, NAME_, "Pre" + DESCRIPTION_, currentConsumer);
        prepareAndSaveTasks(6, false, NAME_, "NotMatch", currentConsumer);
        prepareAndSaveTasks(8, false, NAME_, "Pre" + DESCRIPTION_, someConsumerWithTasks);
        prepareAndSaveTasks(6, false, NAME_, "NotMatch", someConsumerWithTasks);
        assertThat(dao.findAllByConsumerAndNameOrDescription(currentConsumer.getId(), DESCRIPTION_).size()).isEqualTo(8L);

        prepareAndSaveTasks(15, true, NAME_, DESCRIPTION_ + "Suf", currentConsumer);
        prepareAndSaveTasks(1, true, NAME_, "notMatch", currentConsumer);
        prepareAndSaveTasks(15, true, NAME_, DESCRIPTION_ + "Suf", someConsumerWithTasks);
        prepareAndSaveTasks(1, true, NAME_, "notMatch", someConsumerWithTasks);
        assertThat(dao.findAllByConsumerAndNameOrDescription(currentConsumer.getId(), DESCRIPTION_).size()).isEqualTo(23L);
    }

    @Test
    public void findAllByUserAndDayCountTest() {
        assertThat(dao.findAllByConsumerAndDayCount(currentConsumer.getId(), MONDAY)).isEqualTo(0L);

        prepareAndSaveTasks(10, false, currentConsumer, true);
        prepareAndSaveTasks(10, false, someConsumerWithTasks, true);
        assertThat(dao.findAllByConsumerAndDayCount(currentConsumer.getId(), MONDAY)).isEqualTo(10L);

        prepareAndSaveTasks(3, true, currentConsumer, true);
        prepareAndSaveTasks(3, true, someConsumerWithTasks, true);
        assertThat(dao.findAllByConsumerAndDayCount(currentConsumer.getId(), MONDAY)).isEqualTo(13L);
    }

    @Test
    public void findAllByUserAndDayTest() {
        assertThat(dao.findAllByConsumerAndDay(currentConsumer.getId(), MONDAY, 0, 10).size()).isEqualTo(0);

        prepareAndSaveTasks(7, false, currentConsumer, true);
        prepareAndSaveTasks(7, false, someConsumerWithTasks, true);
        assertThat(dao.findAllByConsumerAndDay(currentConsumer.getId(), MONDAY, 0, 10).size()).isEqualTo(7);

        prepareAndSaveTasks(7, false, currentConsumer, true);
        prepareAndSaveTasks(7, false, someConsumerWithTasks, true);
        assertThat(dao.findAllByConsumerAndDay(currentConsumer.getId(), MONDAY, 0, 10).size()).isEqualTo(10);

        assertThat(dao.findAllByConsumerAndDay(currentConsumer.getId(), MONDAY, 10, 10).size()).isEqualTo(4);

        prepareAndSaveTasks(7, false, currentConsumer, true);
        prepareAndSaveTasks(7, false, someConsumerWithTasks, true);
        assertThat(dao.findAllByConsumerAndDay(currentConsumer.getId(), MONDAY, 10, 10).size()).isEqualTo(10);

    }

    private void prepareAndSaveTasks(int amount, boolean isCompleted, Consumer consumer) {
        for (int indx = 0; indx < amount; indx++) {
            Task task = new Task()
                    .setName(NAME_ + indx)
                    .setDescription(DESCRIPTION_ + indx)
                    .setCompleted(isCompleted)
                    .setConsumer(consumer);
            dao.save(task);
            task.getDays().add(new Weekday().setTask(task).setDay(MONDAY));
            task.getDays().add(new Weekday().setTask(task).setDay(TUESDAY));
            task.getDays().add(new Weekday().setTask(task).setDay(WEDNESDAY));
            task.getDays().add(new Weekday().setTask(task).setDay(THURSDAY));
            task.getDays().add(new Weekday().setTask(task).setDay(FRIDAY));
            task.getDays().add(new Weekday().setTask(task).setDay(SATURDAY));
            task.getDays().add(new Weekday().setTask(task).setDay(SUNDAY));

            dao.update(task);
            consumerDao.update(consumer.addTask(task));
        }
    }
    private void prepareAndSaveTasks(int amount, boolean isCompleted, Consumer consumer, boolean isMondayEnabled) {
        for (int indx = 0; indx < amount; indx++) {
            Task task = new Task()
                    .setName(NAME_ + indx)
                    .setDescription(DESCRIPTION_ + indx)
                    .setCompleted(isCompleted)
                    .setConsumer(consumer);
            dao.save(task);
            task.getDays().add(new Weekday().setTask(task).setEnabled(isMondayEnabled).setDay(MONDAY));
            task.getDays().add(new Weekday().setTask(task).setDay(TUESDAY));
            task.getDays().add(new Weekday().setTask(task).setDay(WEDNESDAY));
            task.getDays().add(new Weekday().setTask(task).setDay(THURSDAY));
            task.getDays().add(new Weekday().setTask(task).setDay(FRIDAY));
            task.getDays().add(new Weekday().setTask(task).setDay(SATURDAY));
            task.getDays().add(new Weekday().setTask(task).setDay(SUNDAY));

            dao.update(task);
            consumerDao.update(consumer.addTask(task));
        }
    }

    private void prepareAndSaveTasks(int amount, boolean isCompleted, String name, String description, Consumer consumer) {
        for (int indx = 0; indx < amount; indx++) {

            Task task = new Task()
                    .setName(name + indx)
                    .setDescription(description + indx)
                    .setCompleted(isCompleted)
                    .setConsumer(consumer);

            dao.save(task);
            task.getDays().add(new Weekday().setTask(task).setDay(MONDAY));
            task.getDays().add(new Weekday().setTask(task).setDay(TUESDAY));
            task.getDays().add(new Weekday().setTask(task).setDay(WEDNESDAY));
            task.getDays().add(new Weekday().setTask(task).setDay(THURSDAY));
            task.getDays().add(new Weekday().setTask(task).setDay(FRIDAY));
            task.getDays().add(new Weekday().setTask(task).setDay(SATURDAY));
            task.getDays().add(new Weekday().setTask(task).setDay(SUNDAY));

            dao.update(task);
            consumerDao.update(consumer.addTask(task));
        }
    }
}