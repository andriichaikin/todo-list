package ua.todo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import ua.todo.Application;
import ua.todo.pojo.CompareResult;
import ua.todo.pojo.Task;
import ua.todo.service.TaskService;
import ua.todo.util.TaskUtils;
import ua.todo.utils.HttpClientUtils;

import java.io.IOException;
import java.util.Optional;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.*;
import static ua.todo.utils.HttpClientUtils.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TaskControllerTest {

    private static final long DEFAULT_CONSUMER_ID = 777L;
    private static final int DEFAULT_START = 0;
    private static final int DEFAULT_AMOUNT = 10;
    private static final boolean DEFAULT_IS_COMPLETED = false;

    @LocalServerPort
    private int port;

    @Autowired
    @InjectMocks
    private TaskController controller;

    @Mock
    private TaskService taskService;

    private HttpClientUtils client;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        client = wrapAuth(new HttpClientUtils(port));
    }

    @Test
    public void foundByIdTest() {
        long id = 0L;
        Optional<Task> task = TaskUtils.getById(id);
        assert task.isPresent();

        when(taskService.findById(id)).thenReturn(task);

        Request request = client.request("/task/find/" + id).build();
        Response response = client.call(request);

        assertThat(response.code()).isEqualTo(OK.value());
        assertThat(read(response.body(), Task.class)).isNotNull();
        verify(taskService, times(1)).findById(id);
    }

    @Test
    public void notFoundByIdTest() throws IOException {
        long id = 0L;
        when(taskService.findById(id)).thenReturn(Optional.empty());

        Request request = client.request("/task/find/" + id).build();

        Response response = client.call(request);

        assertThat(response.code()).isEqualTo(NO_CONTENT.value());
        assertThat(response.body().string()).isEmpty();
        verify(taskService, times(1)).findById(id);
    }

    @Test
    public void findAllWithCompletedFlagTrueTest() {
        when(taskService
                .findAllByConsumerBatch(DEFAULT_CONSUMER_ID, DEFAULT_START, DEFAULT_AMOUNT, DEFAULT_IS_COMPLETED))
                .thenReturn(TaskUtils.getTask(DEFAULT_START, DEFAULT_AMOUNT, DEFAULT_IS_COMPLETED));

        Request request = client.request(
                format("/task/find/all?consumerId=%s&start=%s&amount=%s&completedflag=%s&completed=%s",
                        DEFAULT_CONSUMER_ID, DEFAULT_START, DEFAULT_AMOUNT, true, DEFAULT_IS_COMPLETED))
                .build();

        Response response = client.call(request);

        assertThat(response.code()).isEqualTo(OK.value());
        assertThat(readList(response.body(), Task.class).size())
                .isEqualTo(DEFAULT_AMOUNT);
        verify(taskService, times(1))
                .findAllByConsumerBatch(DEFAULT_CONSUMER_ID, DEFAULT_START, DEFAULT_AMOUNT, DEFAULT_IS_COMPLETED);
    }

    @Test
    public void findAllWithCompletedFlagFalseTest() {
        when(taskService
                .findAllByConsumerBatch(DEFAULT_CONSUMER_ID, DEFAULT_START, DEFAULT_AMOUNT))
                .thenReturn(TaskUtils.getTask(DEFAULT_START, DEFAULT_AMOUNT));

        Request request = client.request(
                format("/task/find/all?consumerId=%s&start=%s&amount=%s&completedflag=%s&completed=%s",
                        DEFAULT_CONSUMER_ID, DEFAULT_START, DEFAULT_AMOUNT, false, DEFAULT_IS_COMPLETED))
                .build();

        Response response = client.call(request);

        assertThat(response.code()).isEqualTo(OK.value());
        assertThat(readList(response.body(), Task.class).size())
                .isEqualTo(DEFAULT_AMOUNT);
        verify(taskService, times(1))
                .findAllByConsumerBatch(DEFAULT_CONSUMER_ID, DEFAULT_START, DEFAULT_AMOUNT);
    }

    @Test
    public void findAllWithDefaultParamsTest() {
        when(taskService
                .findAllByConsumerBatch(DEFAULT_CONSUMER_ID, DEFAULT_START, DEFAULT_AMOUNT))
                .thenReturn(TaskUtils.getTask(DEFAULT_START, DEFAULT_AMOUNT));

        Request request = client
                .request(format("/task/find/all?consumerId=%s&", DEFAULT_CONSUMER_ID))
                .build();

        Response response = client.call(request);

        assertThat(response.code()).isEqualTo(OK.value());
        assertThat(readList(response.body(), Task.class).size()).isEqualTo(DEFAULT_AMOUNT);
        verify(taskService, times(1))
                .findAllByConsumerBatch(DEFAULT_CONSUMER_ID, DEFAULT_START, DEFAULT_AMOUNT);
    }

    @Test
    public void findAllWithDefaultCompletedFlagTest() {
        when(taskService
                .findAllByConsumerBatch(DEFAULT_CONSUMER_ID, DEFAULT_START, DEFAULT_AMOUNT))
                .thenReturn(TaskUtils.getTask(DEFAULT_START, DEFAULT_AMOUNT));

        Request request = client.request(
                format("/task/find/all?consumerId=%s&start=%s&amount=%s&completed=%s",
                        DEFAULT_CONSUMER_ID, DEFAULT_START, DEFAULT_AMOUNT, DEFAULT_IS_COMPLETED))
                .build();

        Response response = client.call(request);

        assertThat(response.code()).isEqualTo(OK.value());
        assertThat(readList(response.body(), Task.class).size())
                .isEqualTo(DEFAULT_AMOUNT);
        verify(taskService, times(1))
                .findAllByConsumerBatch(DEFAULT_CONSUMER_ID, DEFAULT_START, DEFAULT_AMOUNT);
    }

    @Test
    public void findAllWithDefaultPaginationParamsTest() {
        when(taskService
                .findAllByConsumerBatch(DEFAULT_CONSUMER_ID, DEFAULT_START, DEFAULT_AMOUNT, DEFAULT_IS_COMPLETED))
                .thenReturn(TaskUtils.getTask(DEFAULT_START, DEFAULT_AMOUNT, DEFAULT_IS_COMPLETED));

        Request request = client
                .request(format("/task/find/all?consumerId=%s&completedflag=%s", DEFAULT_CONSUMER_ID, true))
                .build();

        Response response = client.call(request);

        assertThat(response.code()).isEqualTo(OK.value());
        assertThat(readList(response.body(), Task.class).size())
                .isEqualTo(DEFAULT_AMOUNT);
        verify(taskService, times(1))
                .findAllByConsumerBatch(DEFAULT_CONSUMER_ID, DEFAULT_START, DEFAULT_AMOUNT, DEFAULT_IS_COMPLETED);
    }

    @Test
    public void findAllCountWithCompletedFlagTrueTest() throws IOException {
        when(taskService.findAllByConsumerCount(DEFAULT_CONSUMER_ID, true)).thenReturn(22L);

        Request request = client
                .request(format("/task/find/all/count?consumerId=%s&completedflag=%s&completed=%s",
                        DEFAULT_CONSUMER_ID, true, true))
                .build();

        Response response = client.call(request);

        assertThat(response.code()).isEqualTo(OK.value());
        assertThat(response.body().string()).isEqualTo("22");

        verify(taskService, times(1))
                .findAllByConsumerCount(DEFAULT_CONSUMER_ID, true);
        verify(taskService, times(0))
                .findAllByConsumerCount(DEFAULT_CONSUMER_ID);
    }

    @Test
    public void findAllCountDefaultCompletedFlagTrueTest() throws IOException {
        when(taskService.findAllByConsumerCount(DEFAULT_CONSUMER_ID)).thenReturn(25L);

        Request request = client
                .request(format("/task/find/all/count?consumerId=%s", DEFAULT_CONSUMER_ID))
                .build();

        Response response = client.call(request);

        assertThat(response.code()).isEqualTo(OK.value());
        assertThat(response.body().string()).isEqualTo("25");

        verify(taskService, times(0)).findAllByConsumerCount(anyLong(), anyBoolean());
        verify(taskService, times(1)).findAllByConsumerCount(DEFAULT_CONSUMER_ID);
    }

    @Test
    public void saveTaskTest() throws JsonProcessingException {
        Task task = new Task().setId(777L).setName("createdTask").setDescription("This is created task test").setCompleted(false);

        when(taskService.validateTask(any(Task.class))).thenReturn(new CompareResult());
        doNothing().when(taskService).addTask(anyLong(), any(Task.class));

        String json = new ObjectMapper().writeValueAsString(task);

        RequestBody body = RequestBody.create(APP_JSON_TYPE, json);
        Request request = client
                .request(format("/task/create?consumerId=%s", DEFAULT_CONSUMER_ID))
                .post(body)
                .build();

        Response response = client.call(request);

        assertThat(response.code()).isEqualTo(CREATED.value());

        verify(taskService, times(1)).validateTask(any(Task.class));
        verify(taskService, times(1)).addTask(anyLong(), any(Task.class));
    }

    @Test
    public void saveNotValidTaskTest() throws JsonProcessingException {
        Task task = new Task().setId(777L).setName("").setDescription("This is created task test").setCompleted(false);

        when(taskService.validateTask(any(Task.class))).thenReturn(new CompareResult("mock failed msg"));

        String json = new ObjectMapper().writeValueAsString(task);

        RequestBody body = RequestBody.create(APP_JSON_TYPE, json);
        Request request = client
                .request(format("/task/create?consumerId=%s", DEFAULT_CONSUMER_ID))
                .post(body)
                .build();

        Response response = client.call(request);

        assertThat(response.code()).isEqualTo(CONFLICT.value());

        verify(taskService, times(1)).validateTask(any(Task.class));
        verify(taskService, times(0)).addTask(anyLong(), any(Task.class));
    }

    @Test
    public void updateTaskTest() throws JsonProcessingException {
        Task task = TaskUtils.getById(0L)
                .orElseThrow(() -> new IllegalStateException("Task not found"));

        when(taskService.validateTask(any(Task.class))).thenReturn(new CompareResult());
        doNothing().when(taskService).update(any(Task.class));

        String json = new ObjectMapper().writeValueAsString(task);
        RequestBody body = RequestBody.create(APP_JSON_TYPE, json);
        Request request = client
                .request("/task/update")
                .post(body)
                .build();

        Response response = client.call(request);

        assertThat(response.code()).isEqualTo(NO_CONTENT.value());

        verify(taskService, times(1)).validateTask(any(Task.class));
        verify(taskService, times(1)).update(any(Task.class));
    }

    @Test
    public void updateNotValidTaskTest() throws JsonProcessingException {
        Task task = TaskUtils.getById(0L)
                .orElseThrow(() -> new IllegalStateException("Task not found"));

        when(taskService.validateTask(any(Task.class))).thenReturn(new CompareResult("Not valid task msg"));

        String json = new ObjectMapper().writeValueAsString(task);
        RequestBody body = RequestBody.create(APP_JSON_TYPE, json);
        Request request = client
                .request("/task/update")
                .post(body)
                .build();

        Response response = client.call(request);

        assertThat(response.code()).isEqualTo(CONFLICT.value());

        verify(taskService, times(1)).validateTask(any(Task.class));
        verify(taskService, times(0)).update(any(Task.class));
    }

    @Test
    public void setCompletedTest() {
        doNothing().when(taskService).toggleCompleted(1L);

        Request request = client.request("/task/1/update/completed/toggle").build();
        Response response = client.call(request);

        assertThat(response.code()).isEqualTo(OK.value());
        verify(taskService, times(1)).toggleCompleted(1L);
    }

    @Test
    public void deleteTaskTest() {
        doNothing().when(taskService).setRemoved(1L);

        Request request = client.request("/task/1/remove").build();
        Response response = client.call(request);

        assertThat(response.code()).isEqualTo(OK.value());
        verify(taskService, times(1)).setRemoved(1L);
    }

    @Test
    public void findAllByDayTest() {
        String day = "monday";
        when(taskService.findAllByConsumerAndDay(DEFAULT_CONSUMER_ID, day, DEFAULT_START, DEFAULT_AMOUNT))
                .thenReturn(TaskUtils.getTask(DEFAULT_START, DEFAULT_AMOUNT));

        Request request = client
                .request(format("/task/find/all/day?consumerId=%s&val=%s", DEFAULT_CONSUMER_ID, day))
                .build();
        Response response = client.call(request);

        assertThat(response.code()).isEqualTo(OK.value());
        verify(taskService, times(1))
                .findAllByConsumerAndDay(DEFAULT_CONSUMER_ID, day, DEFAULT_START, DEFAULT_AMOUNT);
    }

    @Test
    public void findAllByNameOrDescriptionTest() {
        String searchStr = "test";
        when(taskService.findAllByConsumerAndNameOrDescription(DEFAULT_CONSUMER_ID, searchStr))
                .thenReturn(TaskUtils.getTask(0, 2));

        Request request = client
                .request(format("/task/all/search?consumerId=%s&val=%s", DEFAULT_CONSUMER_ID, searchStr))
                .build();
        Response response = client.call(request);

        assertThat(response.code()).isEqualTo(OK.value());
        verify(taskService, times(1))
                .findAllByConsumerAndNameOrDescription(DEFAULT_CONSUMER_ID, searchStr);
    }
}
