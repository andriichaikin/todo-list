package ua.todo.pojo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ua.todo.dao.ConsumerDao;
import ua.todo.dao.TaskDao;

import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static ua.todo.pojo.Weekday.Day.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class TaskTest {

    private Task task;

    @Autowired
    private TaskDao taskDao;

    @Autowired
    private ConsumerDao consumerDao;

    @Before
    public void setUp() {
        Consumer consumer = new Consumer().setLogin("tet").setPassword("pass");
        consumerDao.save(consumer);
        task = new Task().setName("testName").setConsumer(consumer);

        taskDao.save(task);

        task.getDays().add(new Weekday().setTask(task).setDay(MONDAY));
        task.getDays().add(new Weekday().setTask(task).setDay(TUESDAY));
        task.getDays().add(new Weekday().setTask(task).setDay(WEDNESDAY));
        task.getDays().add(new Weekday().setTask(task).setDay(THURSDAY));
        task.getDays().add(new Weekday().setTask(task).setDay(FRIDAY));
        task.getDays().add(new Weekday().setTask(task).setDay(SATURDAY));
        task.getDays().add(new Weekday().setTask(task).setDay(SUNDAY));

        taskDao.update(task);
    }

    @Test
    public void setDayEnableTest() {
        assertThat(task.isDayEnabled("monday")).isFalse();

        task.setDayEnabled("monday", true);
        assertThat(task.isDayEnabled("monday")).isTrue();

        task.setDayEnabled("Monday", false);
        assertThat(task.isDayEnabled("monday")).isFalse();

        task.setDayEnabled("MONDAY", true);
        assertThat(task.isDayEnabled("monday")).isTrue();
    }

    @Test
    public void isDayEnabledTest() {

        assertThat(task.isDayEnabled("tuesday")).isFalse();
        assertThat(task.isDayEnabled("TUESDAY")).isFalse();
        assertThat(task.isDayEnabled("Tuesday")).isFalse();

        task.setDayEnabled("tuesday", true);

        assertThat(task.isDayEnabled("tuesday")).isTrue();
        assertThat(task.isDayEnabled("TUESDAY")).isTrue();
        assertThat(task.isDayEnabled("Tuesday")).isTrue();

        task.setDayEnabled("tuesday", false);

        assertThat(task.isDayEnabled("tuesday")).isFalse();
        assertThat(task.isDayEnabled("TUESDAY")).isFalse();
        assertThat(task.isDayEnabled("Tuesday")).isFalse();
    }

}
