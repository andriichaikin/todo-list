package ua.todo.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ua.todo.dao.ConsumerDao;
import ua.todo.dto.ConsumerInfoDTO;
import ua.todo.pojo.Consumer;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConsumerServiceTest {

    @Autowired
    @InjectMocks
    private ConsumerService userService;

    @Mock
    private ConsumerDao userDao;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findByIdTest() {
        long indx = 0L;
        Consumer user = new Consumer().setId(indx).setName("TestName");
        when(userDao.findById(indx)).thenReturn(Optional.of(user));

        Optional<Consumer> found = userService.findById(indx);

        assertThat(found.isPresent()).isTrue();
        verify(userDao, times(1)).findById(indx);
    }

    @Test
    public void updateUserInfoTest() {
        long indx = 0L;
        Consumer user = new Consumer().setId(indx).setName("testName");

        doNothing().when(userDao).update(user);

        ConsumerInfoDTO uInfo = new ConsumerInfoDTO().setName("Updated").setSurname("updatedSurn");
        userService.updateUserInfo(user, uInfo);

        assertThat(user.getName()).isEqualTo(uInfo.getName());

        verify(userDao, times(1)).update(user);
    }
}
