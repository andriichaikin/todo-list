package ua.todo.pojo;

import java.util.LinkedList;
import java.util.List;

import static java.lang.String.format;

public class CompareResult {

    private List<String> results = new LinkedList<>();

    public CompareResult() {
    }

    public CompareResult(String info) {
        results.add(info);
    }

    public CompareResult(List<String> results) {
        this.results = results;
    }

    public List<String> add(String info) {
        results.add(info);
        return results;
    }

    public List<String> add(String info, Object ...args) {
        results.add(format(info, args));
        return results;
    }

    public boolean isValid() {
        return results.size() == 0;
    }

    public boolean isInvalid() {
        return !isValid();
    }

}
