import {HANDLE_REGISTRATION_INPUT_CHANGES, REGISTRATION_FAILED, REGISTRATION_SUCCESS} from "../../constants";
import API from "../../../utils/API";
import {toggleSpinner} from "../spinner-action";

export const handleRegistrationInput = (key, value) => {
    debugger
    const action = {
        type: HANDLE_REGISTRATION_INPUT_CHANGES,
        key,
        value,
    };
    return action;
};

export const registerUser = user => dispatch => {
    dispatch(toggleSpinner(true));

    API.post('/user/registration', user)
        .then(res => {
            dispatch(__registrationSuccess());
        })
        .catch(err => __registrationFailed())
        .finally(() => dispatch(toggleSpinner(false)));
};

const __registrationSuccess = () => {
    const action = {
        type: REGISTRATION_SUCCESS,
    };
    return action;
};

const __registrationFailed = () => {
    const action = {
        type: REGISTRATION_FAILED,
    };
    return action;
};
