import axios from 'axios'
import {saveToken, getToken, isTokenExist, isExpiredSoon, removeToken} from '../auth/auth-utils'

export const baseUrl = 'http://localhost:8080';
// export const baseUrl = 'http://192.168.0.112:8080';

const instance = axios.create({
    baseURL: `${baseUrl}/api`,
    responseType: 'json',
    headers: {
        'Content-Type': 'application/json',
    }
});

let isAlreadyFetchingToken = false;
instance.interceptors.request.use(request => {

    request.headers['Authorization'] = getToken();

    if (!isAlreadyFetchingToken && isTokenExist() && isExpiredSoon()) {
        isAlreadyFetchingToken = true;

        instance
            .get('/authenticate/refresh')
            .then(response => {
                saveToken(response.data.token);
            })
            .catch(err => {
                console.log(err);
            })
            .finally(() => {
                isAlreadyFetchingToken = false;
            })
    }

    return Promise.resolve(request);
}, error => {
    return error;
});


instance.interceptors.response.use(
    response => response,
    error => {
        if (error.response && error.response.status === 401) {
            console.log('User unauthorized');
            removeToken();
            // window.location.href = '/login';
        }

        return Promise.reject(error);
    });


export default instance;
