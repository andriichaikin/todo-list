import {
    USER_AVATAR_ATTACH_FILE,
    USER_AVATAR_REMOVE_ATTACHED_FILE,
    USER_AVATAR_SET_UPLOADING,
    USER_AVATAR_UPLOAD_SUCCESS
} from '../../constants';

const userAvatarReducer = (state = initialState, action) => {

    switch (action.type) {
        case USER_AVATAR_ATTACH_FILE: {
            state = {...state, file: action.file};
            return state;
        }
        case USER_AVATAR_SET_UPLOADING: {
            state = {...state, isLoading: action.isLoading};
            return state;
        }
        case USER_AVATAR_REMOVE_ATTACHED_FILE: {
            state = {...state, file: undefined};
            return state;
        }
        case USER_AVATAR_UPLOAD_SUCCESS: {
            state = {...state, file: undefined, isLoading: false};
            return state;
        }
        default:
            return state;
    }
};

export default userAvatarReducer;

const initialState = {
    file: undefined,
    isLoading: false,
};
