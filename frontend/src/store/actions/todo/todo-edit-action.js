import API from "../../../utils/API";
import {
    FETCH_TODO_SUCCESS,
    FETCH_TODO_FAILED,
    SET_TODO_SAVE_SUCCESS,
    SET_TODO_SAVE_FAILED,
} from "../../constants";
import {toggleSpinner} from "../spinner-action";

export const saveTodo = ({id, name, description, days, completed}) => (dispatch, getState) => {

    dispatch(toggleSpinner(true));
    API.post('/task/update', {
        id,
        name,
        description,
        days,
        completed
    })
        .then(() => dispatch(__saveTodoSuccess()))
        .catch(() => dispatch(__saveTodoFailed()))
        .finally(() => dispatch(toggleSpinner(false)));
};

const __saveTodoSuccess = () => {
    const action = {
        type: SET_TODO_SAVE_SUCCESS,
        saveSuccess: true,
    };

    return action;
};

const __saveTodoFailed = () => {
    const action = {
        type: SET_TODO_SAVE_FAILED,
        saveFailed: true,
    };
    return action;
};

export const fetchTodo = id => dispatch => {

    dispatch(toggleSpinner(true));
    API.get(`/task/find/${id}`)
        .then(response => {

            const {name, description, days, completed} = response.data;
            dispatch(__fetchTodoSuccess(id, name, description, days, completed));
        })
        .catch(err => dispatch(__fetchTodoError()))
        .finally(() => dispatch(toggleSpinner(false)));
};

const __fetchTodoSuccess = (id, name, description, days, completed) => {
    const action = {
        type: FETCH_TODO_SUCCESS,
        id,
        name,
        description,
        days,
        completed,
    };
    return action;
};

const __fetchTodoError = () => {
    const action = {
        type: FETCH_TODO_FAILED,
    };
    return action;
};
