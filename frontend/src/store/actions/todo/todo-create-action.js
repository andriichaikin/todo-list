import API from "../../../utils/API";
import {SET_TODO_SAVE_SUCCESS, SET_TODO_SAVE_FAILED,} from "../../constants";
import {toggleSpinner} from "../spinner-action";

export const saveTodo = (consumerId, name, description, days) => (dispatch, getState) => {

    dispatch(toggleSpinner(true));
    API.post(`/task/create?consumerId=${consumerId}`, {
        name,
        description,
        days
    })
        .then(response => {
            dispatch(__setSaved());
        })
        .catch(err => {
            dispatch(__setSavedFailed())
        })
        .finally(() => dispatch(toggleSpinner(false)));

};

const __setSaved = () => {
    const action = {
        type: SET_TODO_SAVE_SUCCESS,
        saveSuccess: true,
    };
    return action;
};

const __setSavedFailed = () => {
    const action = {
        type: SET_TODO_SAVE_FAILED,
        saveFailed: true,
    };
    return action;
};
