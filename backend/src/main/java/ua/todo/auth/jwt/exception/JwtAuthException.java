package ua.todo.auth.jwt.exception;

import ua.todo.auth.exception.AuthException;

public class JwtAuthException extends AuthException {

    public JwtAuthException(String message) {
        super(message);
    }
}
