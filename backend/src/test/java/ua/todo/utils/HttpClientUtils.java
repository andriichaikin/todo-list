package ua.todo.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import org.springframework.http.HttpStatus;
import ua.todo.auth.jwt.pojo.JWTTokenRequest;
import ua.todo.auth.jwt.pojo.JwtTokenResponse;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import static java.lang.String.format;
import static org.springframework.util.StringUtils.isEmpty;

public class HttpClientUtils {

    public static final MediaType APP_JSON_TYPE = MediaType.parse("application/json; charset=utf-8");
    private static final OkHttpClient.Builder builder = new OkHttpClient.Builder();

    private final String base_url;

    public HttpClientUtils(int port) {
        if (port <= 0) throw new IllegalArgumentException("Http port is less or equal 0");

        base_url = format("http://localhost:%s/api", port);
    }

    @SuppressWarnings("unchecked")
    public static <T> T read(ResponseBody body, Class<T> targetClass) {
        try {
            String string = body.string();

            return (T) new ObjectMapper().readValue(string, targetClass);
        } catch (IOException e) {
            throw new IllegalStateException(format("Can not read response body. %s", e.getMessage()));
        }
    }

    @SuppressWarnings("ConstantConditions")
    public static <T> List<T> readList(ResponseBody body, T targetClass) {
        TypeReference tRef = new TypeReference<List<T>>() {
        };

        try {
            String string = body.string();

            return new ObjectMapper().readValue(string, tRef);
        } catch (IOException e) {
            throw new IllegalStateException(format("Can not read response body. %s", e.getMessage()));
        }
    }

    public static HttpClientUtils wrapAuth(HttpClientUtils httpClient) {
        if (httpClient == null) throw new IllegalArgumentException("Clien is null");

        try {
            JWTTokenRequest user = new JWTTokenRequest("jonny", "password");
            String json = new ObjectMapper().writeValueAsString(user);
            RequestBody body = RequestBody.create(APP_JSON_TYPE, json);

            Request request = httpClient
                    .request("/authenticate")
                    .post(body)
                    .build();
            Response response = httpClient.call(request);

            if (response.code() == HttpStatus.OK.value()) {
                JwtTokenResponse jwtResponse = read(response.body(), JwtTokenResponse.class);

                String token = jwtResponse.getToken();

                builder.interceptors().add(chain -> {
                    Request original = chain.request();

                    Request authorization = original.newBuilder()
                            .header("Authorization", token)
                            .build();

                    return chain.proceed(authorization);
                });

                return httpClient;
            }
        } catch (Exception e) {
            throw new IllegalStateException("Can not get auth token: " + e.getMessage());
        }

        throw new IllegalStateException("Can not get client with authorization header");
    }

    public OkHttpClient client() {
        return builder.build();
    }

    public Response call(Request request) {

        try {
            return client().newCall(request).execute();
        } catch (IOException e) {
            throw new IllegalStateException(format("Http request exception: %s", e.getMessage()));
        }
    }

    public Request.Builder request(String url) {
        if (isEmpty(url)) throw new IllegalArgumentException("Url is null or empty");

        return new Request.Builder().url(base_url + url);
    }

    public Request.Builder request(URI url) {
        if (url == null) throw new IllegalArgumentException("Url is null");
        return request(url.toString());
    }
}
