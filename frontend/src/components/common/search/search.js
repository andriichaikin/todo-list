import React from 'react';

import './search.css';

const Search = (props) => {

    const {val, placeholder, onChange, classNames = ''} = props;

    const styles = `search__component ${classNames}`;

    return <input type="text"
                  className={styles}
                  value={val}
                  placeholder={placeholder}
                  onChange={onChange}/>

};

export default Search;
