package ua.todo.pojo;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CompareResultTest {

    @Test
    public void addTest() {
        CompareResult cr = new CompareResult();

        assertThat(cr.isValid()).isTrue();
        assertThat(cr.isInvalid()).isFalse();

        cr.add("Test failed text");

        assertThat(cr.isValid()).isFalse();
        assertThat(cr.isInvalid()).isTrue();
    }
}
