package ua.todo.pojo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

import static javax.persistence.GenerationType.*;

@MappedSuperclass
public class BaseEntity<T> implements Serializable {

    private static final long serialVersionUID = 777777777L;

    public static final String ID_FIELD = "id";

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = ID_FIELD)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    public Long getId() {
        return id;
    }

    public T setId(Long id) {
        this.id = id;
        return (T) this;
    }

    @Override
    public String toString() {
        return "BaseEntity{" +
                "id=" + id +
                '}';
    }
}
