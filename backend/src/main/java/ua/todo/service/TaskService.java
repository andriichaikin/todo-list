package ua.todo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.todo.dao.TaskDao;
import ua.todo.pojo.CompareResult;
import ua.todo.pojo.Consumer;
import ua.todo.pojo.Task;
import ua.todo.pojo.Weekday;

import java.util.List;
import java.util.Optional;

import static java.lang.String.format;
import static org.springframework.util.StringUtils.isEmpty;

@Service
public class TaskService {

    @Autowired
    private TaskDao taskDao;

    @Autowired
    private ConsumerService consumerService;

    public Optional<Task> findById(long id) {
        return taskDao.findById(id);
    }

    public List<Task> findAllByConsumerBatch(Long consumerId, int start, int amount) {
        return taskDao.findAllByConsumerBatch(consumerId, start, amount);
    }

    public List<Task> findAllByConsumerBatch(Long consumerId, int start, int amount, boolean isCompleted) {
        return taskDao.findAllByConsumerBatch(consumerId, start, amount, isCompleted);
    }

    public CompareResult validateTask(Task task) {
        CompareResult cr = new CompareResult();

        if (isEmpty(task.getName()) || task.getName().length() < 3)
            cr.add("Task is empty or length less then 3 characters");

        return cr;
    }

    public void addTask(Long consumerId, Task task) {
        Consumer consumer = consumerService.findById(consumerId)
                .orElseThrow(() -> new IllegalStateException(format("can not find consumer by id %s", consumerId)));

        List<Task> tasks = consumer.getTasks();
        tasks.add(task.setConsumer(consumer));
        consumerService.update(consumer);

        System.out.println("done");
    }

    public void update(Task task) {
        if (task == null || task.getId() == null) throw new IllegalArgumentException("Task or id is null");

        Task found = taskDao
                .findById(task.getId())
                .orElseThrow(() -> new IllegalStateException(format("Can not find task by id %s", task.getId())));

        found.getDays().forEach(toUpdate -> {
            Optional<Weekday> updated = task.getDays().stream()
                    .filter(tDay ->
                            tDay.getDay().equals(toUpdate.getDay()))
                    .findFirst();

            updated.ifPresent(d -> toUpdate.setEnabled(d.isEnabled()));
        });

        taskDao.update(found
                .setName(task.getName())
                .setDescription(task.getDescription())
                .setCompleted(task.isCompleted())
                .setRemoved(task.isRemoved()));
    }

    public long findAllByConsumerCount(Long consumerId) {
        return taskDao.findAllByConsumerCount(consumerId);
    }

    public long findAllByConsumerCount(Long consumerId, boolean isCompleted) {
        return taskDao.findAllByConsumerCount(consumerId, isCompleted);
    }

    public void toggleCompleted(Long id) {
        Task task = taskDao.findById(id)
                .orElseThrow(() -> new IllegalStateException("Can not find task by id " + id));

        taskDao.update(task.setCompleted(!task.isCompleted()));
    }

    public void setRemoved(Long id) {
        Task task = taskDao.findById(id)
                .orElseThrow(() -> new IllegalStateException("Can not find task by id " + id));

        taskDao.update(task.setRemoved(true));
    }

    public List<Task> findAllByConsumerAndNameOrDescription(Long consumerId, String val) {
        return taskDao.findAllByConsumerAndNameOrDescription(consumerId, val);
    }

    public List<Task> findAllByConsumerAndDay(Long consumerId, String day, int start, int amount) {
        Weekday.Day weekday = Weekday.Day.valueOf(day);
        return taskDao.findAllByConsumerAndDay(consumerId, weekday, start, amount);
    }
}
