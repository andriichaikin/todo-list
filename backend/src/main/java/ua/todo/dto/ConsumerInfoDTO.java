package ua.todo.dto;

public class ConsumerInfoDTO {

    private String name;
    private String surname;
    private String email;
    private String phone;

    public String getName() {
        return name;
    }

    public ConsumerInfoDTO setName(String name) {
        this.name = name;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public ConsumerInfoDTO setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public ConsumerInfoDTO setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public ConsumerInfoDTO setPhone(String phone) {
        this.phone = phone;
        return this;
    }
}
