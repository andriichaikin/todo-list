export const DAYS_OF_WEEK = [
    {day: 'SUNDAY', enabled: false},
    {day: 'MONDAY', enabled: false},
    {day: 'TUESDAY', enabled: false},
    {day: 'WEDNESDAY', enabled: false},
    {day: 'THURSDAY', enabled: false},
    {day: 'FRIDAY', enabled: false},
    {day: 'SATURDAY', enabled: false},
];

export const DEFAULT_SETTINGS = {
    lang: 'en',
};
