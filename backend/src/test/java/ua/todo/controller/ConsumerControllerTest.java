package ua.todo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import ua.todo.Application;
import ua.todo.dto.ConsumerInfoDTO;
import ua.todo.pojo.Consumer;
import ua.todo.service.ConsumerService;
import ua.todo.utils.HttpClientUtils;

import java.io.IOException;
import java.util.Optional;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.OK;
import static ua.todo.utils.HttpClientUtils.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = RANDOM_PORT)
public class ConsumerControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    @InjectMocks
    private ConsumerController consumerController;

    @Mock
    private ConsumerService consumerService;

    private HttpClientUtils client;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        client = wrapAuth(new HttpClientUtils(port));
    }

    @Test
    public void getUserInfoTest() {
        Consumer user = new Consumer().setId(0L).setName("name");
        when(consumerService.findById(user.getId())).thenReturn(Optional.of(user));

        Request request = client
                .request(format("/user/%s/get", user.getId()))
                .build();

        Response response = client.call(request);
        assertThat(response.code()).isEqualTo(OK.value());

        ConsumerInfoDTO uInfo = read(response.body(), ConsumerInfoDTO.class);
        assertThat(uInfo.getName()).isEqualTo(user.getName());

        verify(consumerService, times(1)).findById(user.getId());
    }

    @Test
    public void userInfoNotFoundTest() throws IOException {
        long indx = 0L;
        when(consumerService.findById(indx)).thenReturn(Optional.empty());

        Request request = client
                .request(format("/user/%s/get", indx))
                .build();

        Response response = client.call(request);
        assertThat(response.code()).isEqualTo(CONFLICT.value());

//        assertThat(response.body().string()).isEmpty();

        verify(consumerService, times(1)).findById(indx);
    }

    @Test
    public void updateUserInfoTest() throws JsonProcessingException {
        Consumer user = new Consumer().setId(0L).setName("mockName");

        ConsumerInfoDTO userInfo = new ConsumerInfoDTO().setName("updatedName").setSurname("new Surname");

        when(consumerService.findById(user.getId())).thenReturn(Optional.of(user));
        doNothing().when(consumerService).updateUserInfo(user, userInfo);

        String json = new ObjectMapper().writeValueAsString(user);
        RequestBody body = RequestBody.create(APP_JSON_TYPE, json);
        Request request = client.request(format("/user/%s/update", user.getId()))
                .post(body)
                .build();

        Response response = client.call(request);
        assertThat(response.code()).isEqualTo(OK.value());

        verify(consumerService, times(1)).findById(user.getId());
        verify(consumerService, times(1)).updateUserInfo(any(Consumer.class), any(ConsumerInfoDTO.class));
    }

    @Test
    public void updateUserInfoNotFoundTest() throws JsonProcessingException {
        long indx = 0L;
        ConsumerInfoDTO userInfo = new ConsumerInfoDTO().setName("updatedName").setSurname("new Surname");

        when(consumerService.findById(indx)).thenReturn(Optional.empty());

        String json = new ObjectMapper().writeValueAsString(userInfo);
        RequestBody body = RequestBody.create(APP_JSON_TYPE, json);
        Request request = client.request(format("/user/%s/update", indx))
                .post(body)
                .build();

        Response response = client.call(request);
        assertThat(response.code()).isEqualTo(CONFLICT.value());

        verify(consumerService, times(1)).findById(indx);
        verify(consumerService, times(0)).updateUserInfo(any(Consumer.class), any(ConsumerInfoDTO.class));
    }
}
