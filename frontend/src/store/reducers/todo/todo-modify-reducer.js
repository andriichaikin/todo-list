import {DAYS_OF_WEEK} from '../../../utils/constants';
import {
    FETCH_TODO_FAILED,
    FETCH_TODO_SUCCESS,
    RESET_LOCAL_TODO_DATA, SET_TODO_SAVE_FAILED, SET_TODO_SAVE_SUCCESS,
    UPDATE_TODO_DAY,
    UPDATE_TODO_PROPERTY
} from "../../constants";


const todoModifyReducer = (state = initialState, action) => {

    switch (action.type) {
        case FETCH_TODO_SUCCESS: {
            const {id, name, description, days, completed} = action;
            state = {
                ...state,
                todo: {id, name, description, days, completed}
            };
            return state;
        }
        case FETCH_TODO_FAILED: {
            /*todo */
            return state;
        }
        case SET_TODO_SAVE_SUCCESS: {
            state = {
                ...state,
                info: {
                    ...state.info,
                    saveSuccess: action.saveSuccess,
                    saveFailed: false,
                    errorMsg: '',
                }
            };

            return state;
        }
        case SET_TODO_SAVE_FAILED: {
            state = {
                ...state,
                info: {
                    ...state.info,
                    saveSuccess: false,
                    saveFailed: action.saveFailed,
                    errorMsg: '',
                }
            };

            return state;
        }
        case UPDATE_TODO_PROPERTY: {
            state = {
                ...state,
                todo: {
                    ...state.todo,
                    [action.key]: action.value
                }
            };
            return state;
        }
        case UPDATE_TODO_DAY: {
            const indx = state.todo
                .days
                .findIndex(item => item.day === action.key);

            state.todo.days[indx].enabled = action.checked;

            state = {
                ...state,
                todo: {
                    ...state.todo,
                    days: [...state.todo.days],
                }
            };
            return state;
        }
        case RESET_LOCAL_TODO_DATA: {
            state = getInitialState();
            return state;
        }
        default:
            return state;
    }
};

export default todoModifyReducer;

function getInitialState() {
    const days = DAYS_OF_WEEK.map(d => ({...d}));

    return {
        todo: {
            id: undefined,
            name: '',
            description: '',
            days,
            completed: undefined,
        },
        info: {
            saveSuccess: false,
            saveFailed: false,
            errorMsg: '',
        },
    };
}

const initialState = getInitialState();
