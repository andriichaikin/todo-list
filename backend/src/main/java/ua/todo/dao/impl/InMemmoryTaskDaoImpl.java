//package ua.todo.dao.impl;
//
//import org.springframework.stereotype.Repository;
//import ua.todo.dao.TaskDao;
//import ua.todo.pojo.Task;
//import ua.todo.pojo.Weekday;
//import ua.todo.util.TaskUtils;
//
//import java.util.List;
//import java.util.Optional;
//
//import static java.lang.String.format;
//
//@Repository
//public class InMemmoryTaskDaoImpl implements TaskDao {
//
//    public Optional<Task> findById(long id) {
//        return TaskUtils.getById(id);
//    }
//
//    public List<Task> findAllByConsumerBatch(int start, int amount) {
//        return TaskUtils.getTask(start, amount);
//    }
//
//    public List<Task> findAllByConsumerBatch(int start, int amount, boolean isCompleted) {
//
//        return TaskUtils.getTask(start, amount, isCompleted);
//    }
//
//    public Long addTask(Task task) {
//        Long id = TaskUtils.getLastId();
//
//        task.setId(++id).setCompleted(false);
//
//        TaskUtils.add(task);
//
//        return task.getId();
//    }
//
//    public void update(Task task) {
//        Optional<Task> founded = TaskUtils.getById(task.getId());
//
//        if (founded.isPresent()) {
//            founded.get()
//                    .setName(task.getName())
//                    .setDescription(task.getDescription())
//                    .setCompleted(task.isCompleted())
//                    .setDays(task.getDays());
//        }
//
//        throw new IllegalStateException(format("Couldn`t update entity with id [%s]. Reason: task not found", task.getId()));
//    }
//
//    public Long findAllByConsumerCount() {
//        return TaskUtils.findAllByConsumerCount();
//    }
//
//    public Long findAllByConsumerCount(boolean isCompleted) {
//        return TaskUtils.findAllByConsumerCount(isCompleted);
//    }
//
//    public List<Task> findAllByConsumerAndNameOrDescription(String val) {
//        return TaskUtils.findAllByConsumerAndNameOrDescription(val);
//    }
//
//    public Long findAllByConsumerAndDayCount(Weekday.Day day) {
//        return TaskUtils.findAllByDay(day.value(), );
//    }
//
//    public List<Task> findAllByConsumerAndDay(Weekday.Day day, int start, int amount) {
//
//        return TaskUtils.findAllByDay(day.value(), start, amount);
//    }
//
//    @Override
//    public Long findAllByConsumerAndNameOrDescriptionCount(String val) {
//        return null;
//    }
//
//
//}
