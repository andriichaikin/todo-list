package ua.todo.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
import ua.todo.auth.jwt.JwtTokenUtils;
import ua.todo.auth.jwt.pojo.JWTTokenRequest;
import ua.todo.auth.jwt.pojo.JwtTokenResponse;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@CrossOrigin
public class AuthController {

    @Value("${jwt.http.request.header}")
    private String tokenHeader;

    @Value("${jwt.http.request.header.prefix}")
    private String headerPrefix;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    @Qualifier("inMemoryConsumerDetailsService")
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtTokenUtils jwtTokenUtils;


    @RequestMapping(value = "${jwt.create.token.url}", consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity createAuthToken(@RequestBody JWTTokenRequest jwtRequest) {
        try {
            authenticate(jwtRequest.getUsername(), jwtRequest.getPassword());

            UserDetails userDetails = userDetailsService.loadUserByUsername(jwtRequest.getUsername());

            String token = jwtTokenUtils.generateToken(userDetails);

            return ResponseEntity.ok(new JwtTokenResponse(headerPrefix + token));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return ResponseEntity.status(UNAUTHORIZED).build();
    }

    @RequestMapping(value = "${jwt.refresh.token.url}", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity refreshToken(HttpServletRequest request) {
        String authToken = request.getHeader(tokenHeader);

        String token = authToken.substring(7);

        String username = jwtTokenUtils.getUsernameFromToken(token);

        UserDetails user = userDetailsService.loadUserByUsername(username);
        if (user != null && jwtTokenUtils.canTokenBeRefreshed(token)) {
            String refreshed = jwtTokenUtils.refreshToken(token);
            return ResponseEntity.ok(new JwtTokenResponse(headerPrefix + refreshed));
        }

        return ResponseEntity.status(UNAUTHORIZED).build();
    }

    @ExceptionHandler({AuthenticationException.class})
    public ResponseEntity handleAuthException(AuthenticationException e) {
        return ResponseEntity.status(UNAUTHORIZED).build();
    }

    private void authenticate(String username, String password) {
        Objects.requireNonNull(username, "Name can not be null");
        Objects.requireNonNull(password, "Password can not be null");

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
    }

}
