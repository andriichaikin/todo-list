import {FETCH_TODO_LIST, TOGGLE_TODO_COMPLETED} from "../../constants";

const todoListReducer = (state = [], action) => {
    switch (action.type) {
        case FETCH_TODO_LIST: {
            state = [...action.todoList];
            return state;
        }
        case TOGGLE_TODO_COMPLETED: {
            const indx = findIndxById(state, action.id);
            state[indx] = {
                ...state[indx],
                completed: !state[indx].completed
            };

            return [...state];
        }
        default:
            return state;
    }
};

export default todoListReducer;

function findIndxById(arr, id) {
    return arr.findIndex(item => item.id == id);
}
