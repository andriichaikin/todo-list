package ua.todo.pojo;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

@Entity(name = "Consumer")
@Table(name = "consumer")
public class Consumer extends BaseEntity<Consumer>{

    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";

    @Column(name = LOGIN)
    @NotNull
    private String login;

    @Column(name = PASSWORD)
    @NotNull
    private String password;

    @Column(name = NAME)
    private String name;

    @Column(name = SURNAME)
    private String surname;

    @Column(name = EMAIL)
    private String email;

    @Column(name = PHONE)
    private String phone;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "fk_user_settings", unique = true)
    private ConsumerSettings settings;

    @OneToMany(
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            mappedBy = "consumer",
            orphanRemoval = true)
    @JsonManagedReference
    private List<Task> tasks = new LinkedList<>();

    public String getLogin() {
        return login;
    }

    public Consumer setLogin(String login) {
        this.login = login;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Consumer setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getName() {
        return name;
    }

    public Consumer setName(String name) {
        this.name = name;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public Consumer setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Consumer setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public Consumer setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public ConsumerSettings getSettings() {
        return settings;
    }

    public Consumer setSettings(ConsumerSettings settings) {
        this.settings = settings;
        return this;
    }

    public Consumer addTask(Task task) {
        if(task == null) throw new IllegalArgumentException("Task is null");

        tasks.add(task);
        return this;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public Consumer setTasks(List<Task> tasks) {
        this.tasks = tasks;
        return this;
    }

    @Override
    public String toString() {
        return super.toString()
                + "Consumer{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
