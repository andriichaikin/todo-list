package ua.todo.dao.impl;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;

import ua.todo.dao.ConsumerDao;
import ua.todo.pojo.Consumer;

import java.util.Optional;

@Repository
@Transactional
public class ConsumerDaoImpl implements ConsumerDao {

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public void save(Consumer user) {
        sessionFactory.getCurrentSession().save(user);
    }

    @Override
    public void update(Consumer user) {
        sessionFactory.getCurrentSession().update(user);
    }

    @Override
    public Optional<Consumer> findById(Long id) {
        Consumer user = sessionFactory
                .getCurrentSession()
                .get(Consumer.class, id);
        return Optional.ofNullable(user);
    }

    @Override
    public Optional<Consumer> findByLogin(String login) {
        Query<Consumer> query = sessionFactory
                .getCurrentSession()
                .createQuery("from Consumer u where u.login = :login", Consumer.class);
        query.setParameter("login", login);

        Consumer found = query.uniqueResult();
        return Optional.ofNullable(found);
    }
}
