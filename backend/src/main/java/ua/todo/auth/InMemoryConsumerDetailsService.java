package ua.todo.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ua.todo.auth.jwt.JwtUserDetails;
import ua.todo.dao.ConsumerDao;
import ua.todo.pojo.Consumer;

import java.util.Optional;

@Service
public class InMemoryConsumerDetailsService implements UserDetailsService {

    @Autowired
    private ConsumerDao userDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Consumer> found = userDao.findByLogin(username);

        return found.map(user -> new JwtUserDetails(
                user.getId(),
                user.getLogin(),
                user.getPassword(),
                "USER_ROLE")).orElse(null);

    }
}
