import React, {Component} from 'react';

import './error-boundary.css';

class ErrorBoundary extends Component {

    constructor(props) {
        super(props);

        this.state = {
            hasError: false,
            error: null,
            errorInfo: null,
        };
    }

    componentDidCatch(error, errorInfo) {
        this.setState({
            hasError: true,
            error,
            errorInfo,
        });
    }

    render() {
        const {hasError} = this.state;
        if (hasError) {
            const {customRender} = this.props;
            return customRender ? customRender :
                <div className='error_default'>
                    Ooops! An unpredictable error occurred :(
                </div>
        }

        return this.props.children
    }
}

export default ErrorBoundary;
