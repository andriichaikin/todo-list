import {LOGIN_USER, LOGIN_ERROR, LOGOUT_USER, HANDLE_LOGIN_INPUT_CHANGES} from "../constants";

import API from '../../utils/API';

import {decodeToken, isTokenValid, saveToken, removeToken} from "../../auth/auth-utils";
import {toggleSpinner} from "./spinner-action";

export const authRequest = (username, password) => (dispatch, getState) => {

    dispatch(toggleSpinner(true));
    API.post('/authenticate', {username, password})
        .then(res => {
            const {token} = res.data;
            if (!token) throw Error("Invalid token");

            const decoded = decodeToken(token);
            const isAuthenticated = isTokenValid(decoded);

            if (isAuthenticated) {
                dispatch(login(decoded.id, token));
            } else {
                dispatch(loginError());
            }
        })
        .catch(err => {
            dispatch(loginError());
        })
        .finally(() => dispatch(toggleSpinner(false)));
};

export const handleInputChanges = (property, value) => {
    const action = {
        type: HANDLE_LOGIN_INPUT_CHANGES,
        property,
        value,
    };
    return action;
};

export const login = (id, token) => {
    saveToken(token);
    return setAuthorized(LOGIN_USER, id, true);
};

export const logout = () => {
    removeToken();
    return setAuthorized(LOGOUT_USER, undefined)
};

export const loginError = () => {
    removeToken();
    return setAuthorized(LOGIN_ERROR, undefined, false, true);
};

export const setAuthorized = (type, id, isAuthenticated = false, hasError = false) => {
    const action = {
        type,
        id,
        isAuthenticated,
        hasError
    };
    return action;
};
