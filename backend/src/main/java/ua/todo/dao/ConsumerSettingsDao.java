package ua.todo.dao;

import org.springframework.stereotype.Repository;

import ua.todo.pojo.ConsumerSettings;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface ConsumerSettingsDao {

    void saveOrUpdate(ConsumerSettings userSettings);
}
