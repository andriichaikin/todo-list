package ua.todo.dao.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ua.todo.Application;
import ua.todo.dao.ConsumerDao;
import ua.todo.pojo.Consumer;

import javax.transaction.Transactional;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@Transactional
public class ConsumerDaoTest {

    private static final String PASSWORD = "pass";
    private static final String LOGIN = "login";
    private static final String SURNAME = "uSurname";
    private static final String NAME = "uName";

    @Autowired
    private ConsumerDao consumerDao;

    private Consumer consumer;

    @Before
    public void setUp() {
        consumer = new Consumer()
                .setLogin(LOGIN)
                .setPassword(PASSWORD)
                .setName(NAME)
                .setSurname(SURNAME);
    }

    @Test
    public void saveTest() {
        assert consumer.getId() == null;

        consumerDao.save(consumer);

        assertThat(consumer.getId()).isPositive();
    }

    @Test
    public void updateTest() {
        consumerDao.save(consumer);

        consumerDao.update(consumer.setName("Updated"));
        assertThat(consumer.getName()).isEqualTo("Updated");
    }

    @Test
    public void findByIdTest() {
        consumerDao.save(consumer);

        Optional<Consumer> found = consumerDao.findById(consumer.getId());
        assertThat(found.isPresent()).isTrue();
        assertThat(found.get().getName()).isEqualTo(NAME);
    }
}
