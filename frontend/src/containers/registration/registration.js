import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import './registration.css';
import TextInput from "../../components/common/textinput/text-input";
import {FormattedMessage} from "react-intl";
import {handleRegistrationInput, registerUser} from "../../store/actions/user/user-registration-action";
import Button from "../../components/common/button/button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimesCircle} from "@fortawesome/free-solid-svg-icons";
import {Redirect} from "react-router-dom";

class RegistrationPage extends Component {

    state = {
        redirectToLogin: false,
    };

    handleInputChanges = (e) => {
        const {name, value} = e.target;

        const {handleInput} = this.props;
        handleInput(name, value);
    };

    registerUser = () => {
        const {user} = this.props.registration;
        const isValid = this.validateUser(user);

        if (isValid || true) this.props.register(user);
    };

    redirectToLogin = () => {
        this.setState({redirectToLogin: true})
    };

    getRedirectToLogin = () => {
        return this.state.redirectToLogin ? <Redirect to='/login'/> : null;
    };

    getResponseMsg = () => {
        const {isRegistered, hasError} = this.props.registration;

        if (isRegistered) return (
            <span className='registration__status-msg registration__status-msg--success'>
                    You`ve been registered. Please, login in app!
            </span>
        );

        if (hasError) return (
            <span className='registration__status-msg registration__status-msg--failed'>
                Registration error!
            </span>
        );

        return null;
    };

    render() {
        const {login, password, name, surname, email, phone} = this.props.registration.user;

        const loginLabel = <FormattedMessage id='registration.label.login' defaultMessage='Login'/>
        const passwordLabel = <FormattedMessage id='registration.label.password' defaultMessage='Password'/>
        const nameLabel = <FormattedMessage id='registration.label.name' defaultMessage='Name'/>
        const surnameLabel = <FormattedMessage id='registration.label.surname' defaultMessage='Surname'/>
        const emailLabel = <FormattedMessage id='registration.label.email' defaultMessage='Email'/>
        const phoneLabel = <FormattedMessage id='registration.label.phone' defaultMessage='Phone'/>
        const btnName = <FormattedMessage id='registration.btn.name' defaultMessage='Register'/>

        const RenderRedirectToLogin = this.getRedirectToLogin;

        const ResponseMsg = this.getResponseMsg;

        return (
            <Fragment>
                <RenderRedirectToLogin/>
                <div className='registration__body'>
                    <ResponseMsg/>
                    <div className="registration__frame">
                        <FontAwesomeIcon icon={faTimesCircle}
                                         size='2x'
                                         className='registration__close-icon'
                                         onClick={this.redirectToLogin}/>
                        <ul className='registration__fields'>
                            <li><TextInput name='login'
                                           value={login}
                                           label={loginLabel}
                                           type='text'
                                           onChange={this.handleInputChanges}
                                           autoComplete={false}/>
                            </li>
                            <li><TextInput name='password'
                                           value={password}
                                           label={passwordLabel}
                                           type='password'
                                           onChange={this.handleInputChanges}
                                           autoComplete={false}/>
                            </li>
                            <li><TextInput name='name'
                                           value={name}
                                           label={nameLabel}
                                           type='text'
                                           onChange={this.handleInputChanges}
                                           autoComplete={false}/>
                            </li>
                            <li><TextInput name='surname'
                                           value={surname}
                                           label={surnameLabel}
                                           type='text'
                                           onChange={this.handleInputChanges}
                                           autoComplete={false}/>
                            </li>
                            <li><TextInput name='email'
                                           value={email}
                                           label={emailLabel}
                                           type='text'
                                           onChange={this.handleInputChanges}
                                           autoComplete={false}/>
                            </li>
                            <li><TextInput name='phone'
                                           value={phone}
                                           label={phoneLabel}
                                           type='text'
                                           onChange={this.handleInputChanges}
                                           autoComplete={false}/>
                            </li>
                            <li>
                                <Button name={btnName}
                                        cName='registration__btn'
                                        onClick={this.registerUser}/>
                            </li>
                        </ul>
                    </div>
                </div>
            </Fragment>
        )
    }

    validateUser(user) {
        if (!user) return false;
        if (!user.login || user.login.length < 3) return false;
        if (!user.password || user.password.length < 7) return false;

        return user.name && user.name.length > 3;
    }
}

const mapStateToProps = state => ({
    registration: state.userRegistrationReducer,
});

const mapDispatchToProps = dispatch => ({
    handleInput: (key, value) => dispatch(handleRegistrationInput(key, value)),
    register: (user) => dispatch(registerUser(user)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationPage);
