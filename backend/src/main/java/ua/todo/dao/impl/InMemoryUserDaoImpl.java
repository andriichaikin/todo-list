//package ua.todo.dao.impl;
//
//import org.springframework.stereotype.Repository;
//import ua.todo.dao.ConsumerDao;
//import ua.todo.pojo.Consumer;
//
//import java.util.Optional;
//
//@Repository
//public class InMemoryUserDaoImpl implements ConsumerDao {
//
//    private Consumer mockUser = new Consumer().setId(0L).setName("mockName").setSurname("mockSurname")
//            .setLogin("mockNickname").setPassword("mockPassword").setEmail("mock@email.com").setPhone("+12333333");
//
//    public Optional<Consumer> findById(Long id) {
//        return Optional.of(mockUser);
//    }
//
//    public void update(Consumer user) {
//
//        mockUser.setName(user.getName())
//                .setSurname(user.getSurname())
//                .setLogin(user.getLogin())
//                .setPassword(user.getPassword())
//                .setEmail(user.getEmail())
//                .setPhone(user.getPhone());
//    }
//}
