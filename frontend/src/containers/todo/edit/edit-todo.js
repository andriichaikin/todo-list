import React, {Component} from 'react';

import './edit-todo.css';

import {connect} from "react-redux";

import TodoForm from "../../../components/todo/form/todo-form";
import {Redirect} from "react-router-dom";
import {fetchTodo, saveTodo} from "../../../store/actions/todo/todo-edit-action";
import withStateManager from "../../../hoc/todo/modify/modify-todo";
import {baseUrl} from "../../../utils/API";
import ErrorBoundary from "../../../hoc/error/error-boundary";

class EditTodo extends Component {

    componentDidMount() {
        const {id} = this.props.match.params;

        this.props.fetchTodo(id);
    }

    saveTodo = (e) => {
        e.preventDefault();

        const isValid = this.props.validate();
        if (!isValid) {

        }

        const {todo} = this.props;
        this.props.saveTodo(todo);
    };

    handleError = () => {
        /*todo change cat img*/
        return (
            <div className='todos-error--wrapper'>
                     <span className='todos-error__text'>
                        Sorry, an exception occurred while displaying the task list :(
                    </span>
                <img className='todos-error__img'
                     src={`${baseUrl}/images/cat_crying.png`}
                     alt="cat_crying"/>
            </div>
        )
    };

    handleError = () => {
        /*todo change cat img*/
        return (
            <div className='todos-error--wrapper'>
                     <span className='todos-error__text'>
                        Sorry, an exception occurred while modifying task :(
                    </span>
                <img className='todos-error__img'
                     src={`${baseUrl}/images/cat_crying.png`}
                     alt="cat_crying"/>
            </div>
        )
    };

    render() {
        const Error = this.handleError;

        const {handleInputChanges, handleCheckboxChanges, redirectToMain} = this.props;

        const {todo, info} = this.props;
        const {name, description, days, completed} = todo;


        return (
            <ErrorBoundary customRender={<Error/>}>
                <div className='edit-todo'>
                    <TodoForm name={name}
                              description={description}
                              isCompleted={completed}
                              days={days}
                              actionInfo={info}
                              handleInputChanges={handleInputChanges}
                              handleCheckboxChanges={handleCheckboxChanges}
                              closeWithoutSaving={redirectToMain}
                              saveOrUpdateTodo={this.saveTodo}/>
                </div>
            </ErrorBoundary>
        )
    };
}

const mapDispatchToProps = dispatch => ({
    saveTodo: (todo) => dispatch(saveTodo(todo)),
    fetchTodo: id => dispatch(fetchTodo(id)),
});

const Wrapped = withStateManager(EditTodo);

export default connect(null, mapDispatchToProps)(Wrapped);
