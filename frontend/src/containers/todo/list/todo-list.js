import React, {Fragment} from 'react';

import './todo-list.css';

import TodoItem from "../../../components/todo/item/todo-item";
import Search from "../../../components/common/search/search";

import connect from "react-redux/es/connect/connect";
import {fetchTodoList, toggleCompleted} from "../../../store/actions/todo/todo-list-action";

import {DAYS_OF_WEEK} from '../../../utils/constants';
import ErrorBoundary from "../../../hoc/error/error-boundary";

import {baseUrl} from '../../../utils/API';
import {FormattedMessage} from "react-intl";

const TYPE_ALL = 'all';
const TYPE_TODAY = 'today';
const TYPE_IS_COMPLETED = 'completed';

const NAVIGATION = [
    {type: TYPE_ALL, title: TYPE_ALL},
    {type: TYPE_TODAY, title: TYPE_TODAY},
    {type: TYPE_IS_COMPLETED, title: TYPE_IS_COMPLETED}
];

class TodoList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            searchStr: '',
            currentType: TYPE_ALL,
            showDescrId: undefined,
        };
    }

    componentDidMount() {
        const {id} = this.props.auth;
        this.props.fetchTodoList(`/task/find/all?consumerId=${id}`);
    }

    handleNavClick = (selectedType) => {
        const {id} = this.props.auth;
        let url = `/task/find/all`;

        switch (selectedType) {
            case TYPE_ALL: {
                url = `${url}?consumerId=${id}&start=0&amount=10&completedflag=false`;
                break;
            }
            case TYPE_IS_COMPLETED: {
                url = `${url}?consumerId=${id}&start=0&amount=10&completedflag=true&completed=true`;
                break;
            }
            case TYPE_TODAY: {
                const dayIndx = new Date().getDay();
                const today = DAYS_OF_WEEK[dayIndx];
                url = `${url}/day?consumerId=${id}&val=${today.day}`;
                break;
            }
            default:
                break;
        }

        this.props.fetchTodoList(url)
            .then(() => {
                this.setState({
                    currentType: selectedType,
                    searchStr: '',
                });
            })
    };

    sort = (arr) => {
        if (!arr || arr.length < 2) return arr;

        return arr
            .sort((a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()))
            .sort((a, b) =>
                a.isCompleted && !b.isCompleted ? 1 :
                    !a.isCompleted && b.isCompleted ? -1 : 0);
    };

    toggleTodoDescription = (id) => {
        const itemId = this.state.showDescrId === id ? null : id;

        this.setState({showDescrId: itemId});
    };

    toggleCompleted = (id, e) => {
        e.stopPropagation();

        this.props.toggleCompleted(`/task/${id}/update/completed/toggle`, id);
    };


    handleInputChange = (e) => {
        e.preventDefault();
        let searchStr = e.target.value;

        const {fetchTodoList} = this.props;
        const {id} = this.props.auth;

        if (searchStr && searchStr.length > 2) fetchTodoList(`/task/all/search?consumerId=${id}&val=${searchStr}`);

        if (!searchStr) fetchTodoList(`/task/find/all?consumerId=${id}`);

        this.setState({searchStr});
    };

    getSearchTodo = () => {
        return (
            <FormattedMessage id='todo.list.search.placeholder' defaultMessage='Find by name or description...'>
                {placeholder =>
                    <Search val={this.state.searchStr}
                            placeholder={placeholder}
                            onChange={this.handleInputChange}
                            classNames='todos__search'/>}
            </FormattedMessage>
        )
    };

    getTodoNavigation = () => {
        const items = NAVIGATION.map(nav => {
            let highlightNav = nav.type === this.state.currentType ? 'todos__navs--active' : '';
            return (
                <li key={nav.type}
                    className={highlightNav}
                    onClick={this.handleNavClick.bind(this, nav.type)}>
                    <FormattedMessage id={`todo.list.nav.${nav.title}`} defaultMessage={nav.title}/>
                </li>
            )
        });

        return <Fragment>{items}</Fragment>
    };

    getTodoList = () => {
        const {showDescrId} = this.state;

        return this.props.todoList.map((todo) => {
            return <TodoItem key={todo.id}
                             todo={todo}
                             showDescription={showDescrId === todo.id}
                             toggleDescription={this.toggleTodoDescription.bind(this)}
                             toggleIsCompleted={this.toggleCompleted.bind(this)}
                             history={this.props.history}/>
        });
    };

    handleError = () => {
        /*todo change cat img*/
        return (
            <div className='todos-error--wrapper'>
                     <span className='todos-error__text'>
                        Sorry, an exception occurred while displaying the task list :(
                    </span>
                <img className='todos-error__img'
                     src={`${baseUrl}/images/cat_crying.png`}
                     alt="cat_crying"/>
            </div>
        )
    };

    render() {
        const Search = this.getSearchTodo;
        const TodoNavs = this.getTodoNavigation;
        const TodoList = this.getTodoList;
        const Error = this.handleError;

        return (
            <ErrorBoundary customRender={<Error/>}>
                <div className="todos">
                    <div className='todos__wrapper'>
                        <Search/>
                        <ul className="todos__navs">
                            <TodoNavs/>
                        </ul>
                        <ul className="todos__items">
                            <TodoList/>
                        </ul>
                    </div>
                </div>
            </ErrorBoundary>
        )
    }
}


const mapStateToProps = (state) => ({
    todoList: state.todoListReducer,
    auth: state.authReducer,
});

const mapDispatchToProps = (dispatch) => ({
    toggleCompleted: (url, id) => dispatch(toggleCompleted(url, id)),
    fetchTodoList: (url) => dispatch(fetchTodoList(url)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
