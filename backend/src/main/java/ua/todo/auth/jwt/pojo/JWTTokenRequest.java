package ua.todo.auth.jwt.pojo;

import java.io.Serializable;

public class JWTTokenRequest implements Serializable {

    private static final long serialVersionUID = -5616176897013108345L;

    private String username;
    private String password;

    public JWTTokenRequest() {
    }

    public JWTTokenRequest(String name, String password) {
        this.username = name;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
