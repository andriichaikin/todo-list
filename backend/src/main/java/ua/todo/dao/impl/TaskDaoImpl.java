package ua.todo.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

import ua.todo.dao.TaskDao;
import ua.todo.pojo.Task;
import ua.todo.pojo.Weekday;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class TaskDaoImpl implements TaskDao {

    @Autowired
    private SessionFactory sessionFactory;

    public Optional<Task> findById(long id) {
        return Optional.ofNullable(sessionFactory
                .getCurrentSession()
                .get(Task.class, id));
    }

    public Long findAllByConsumerCount(Long consumerId) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("select count(t) " +
                        "from Task t " +
                        "where t.consumer.id = :consumerId");
        query.setParameter("consumerId", consumerId);

        return (Long) query.getSingleResult();
    }

    public List<Task> findAllByConsumerBatch(Long consumerId, int start, int amount) {
        Session session = sessionFactory.getCurrentSession();

        Query<Task> query = session.createQuery(
                "from Task as t " +
                        "where t.consumer.id = :consumerId " +
                        "order by t.name asc", Task.class);
        query.setParameter("consumerId", consumerId);
        return query
                .setFirstResult(start)
                .setMaxResults(amount)
                .getResultList();
    }

    public Long findAllByConsumerCount(Long consumerId, boolean isCompleted) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("select count (t) " +
                        "from Task t " +
                        "where t.consumer.id = :consumerId " +
                        "and t.isCompleted =: completed");
        query.setParameter("completed", isCompleted);
        query.setParameter("consumerId", consumerId);

        return (Long) query.getSingleResult();
    }

    public List<Task> findAllByConsumerBatch(Long consumerId, int start, int amount, boolean isCompleted) {
        Session session = sessionFactory.getCurrentSession();
        Query<Task> query = session.createQuery(
                "from Task as t " +
                        "where t.consumer.id = :consumerId " +
                        "and t.isCompleted =: completed " +
                        "order by t.name asc ", Task.class);
        query.setParameter("consumerId", consumerId);
        query.setParameter("completed", isCompleted);

        return query
                .setFirstResult(start)
                .setMaxResults(amount)
                .getResultList();
    }

    public Long save(Task task) {
        return (Long) sessionFactory.getCurrentSession().save(task);
    }

    public void update(Task task) {
        sessionFactory.getCurrentSession().update(task);
    }

    public Long findAllByConsumerAndNameOrDescriptionCount(Long consumerId, String val) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery(
                        "select count(t) " +
                                "from Task t " +
                                "where t.consumer.id = :consumerId " +
                                "and (t.name like :val or t.description like :val)");
        query.setParameter("consumerId", consumerId);
        query.setParameter("val", "%" + val + "%");


        return (Long) query.getSingleResult();
    }

    public List<Task> findAllByConsumerAndNameOrDescription(Long consumerId, String val) {
        Query<Task> query = sessionFactory.getCurrentSession()
                .createQuery(
                        "from Task t " +
                                "where t.consumer.id = :consumerId " +
                                "and (t.name like :val or t.description like :val) " +
                                "order by t.name asc", Task.class);
        query.setParameter("consumerId", consumerId);
        query.setParameter("val", "%" + val + "%");


        return query.getResultList();
    }

    public Long findAllByConsumerAndDayCount(Long consumerId, Weekday.Day day) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery(
                        "select count(t) " +
                                "from Task t " +
                                "join t.days d " +
                                "where t.consumer.id = :consumerId " +
                                "and d.isEnabled = true " +
                                "and d.day =: val");
        query.setParameter("consumerId", consumerId);
        query.setParameter("val", day);

        return (Long) query.uniqueResult();
    }

    public List<Task> findAllByConsumerAndDay(Long consumerId, Weekday.Day day, int start, int amount) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery(
                        "select t " +
                                "from Task t " +
                                "join t.days d " +
                                "where t.consumer.id = :consumerId " +
                                "and d.day =: val " +
                                "and d.isEnabled = true " +
                                "order by t.name asc");
        query.setParameter("consumerId", consumerId);
        query.setParameter("val", day);
        query.setFirstResult(start);
        query.setMaxResults(amount);

        return query.getResultList();
    }
}