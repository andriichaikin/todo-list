package ua.todo.service;

import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

@Service
public class FileService {

    public boolean dirsExist(String path) {
        return Files.exists(Paths.get(path));
    }

    public void createDirs(String path) {
        try {
            Files.createDirectories(Paths.get(path));
        } catch (IOException ex) {
            throw new IllegalStateException("Can not create directories. Reason: " + ex.getMessage());
        }
    }

    public String encodeFile(String path) {
        File file = new File(path);
        if (!file.exists()) return "";

        try (FileInputStream fileInputStream = new FileInputStream(file)){
            byte[] bytes = new byte[(int) file.length()];
            fileInputStream.read(bytes);

            return Base64.getEncoder().encodeToString(bytes);
        } catch (Exception e) {
            throw new IllegalStateException("Exception while converting file into byte array: ", e);
        }
    }
}
