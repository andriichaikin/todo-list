import API from "../../../utils/API";
import {FETCH_TODO_LIST, TOGGLE_TODO_COMPLETED} from "../../constants";
import {toggleSpinner} from "../spinner-action";

export const fetchTodoList = (url) => (dispatch, getState) => {

    dispatch(toggleSpinner(true));
    return API.get(url)
        .then(response => {
            return new Promise((resolve, rejext) => {
                dispatch(__fetchTodoList(response.data));

                resolve();
            });
        })
        .catch(err => console.log(err))
        .finally(() => dispatch(toggleSpinner(false)));
};


const __fetchTodoList = (todoList) => {

    const action = {
        type: FETCH_TODO_LIST,
        todoList,
    };

    return action;
};

export const toggleCompleted = (url, id) => (dispatch, getState) => {

    dispatch(toggleSpinner(true));
    API.get(`/task/${id}/update/completed/toggle`)
        .then(() => {
            dispatch(__toggleCompleted(id));
        })
        .catch(err => console.log(err))
        .finally(() => dispatch(toggleSpinner(false)));
};

const __toggleCompleted = (id) => {

    const action = {
        type: TOGGLE_TODO_COMPLETED,
        id,
    };

    return action;
};
