import {TOGGLE_SPINNER} from "../constants";

const spinnerReducer = (state = initialState, action) => {

    switch (action.type) {
        case TOGGLE_SPINNER: {
            state = {isLoading: action.isLoading};
            return state;
        }
        default:
            return state;
    }
};

export default spinnerReducer;

const initialState = {
    isLoading: false,
};
