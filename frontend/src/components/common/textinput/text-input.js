import React from 'react';

import './text-input.css';

const TextInput = ({name, label, value = '', type = 'text', onChange, cName = '', autoComplete = 'on'}) => {
    const classes = 'input-wrapper ' + cName;
    const isEnabledCompletion = autoComplete ? 'on' : 'off';
        //todo value, autocompletion bug
    return (
        <div className={classes}>
            <input type={type}
                   name={name}
                   value={value}
                   placeholder='&nbsp;'
                   autoComplete={isEnabledCompletion}
                   onChange={onChange}/>
            <label htmlFor={name}>{label}</label>
        </div>
    )
};

export default TextInput;
