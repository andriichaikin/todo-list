import React from "react";

const Avatar = (props) => {

    const {avatar, size} = props;
    const source = avatar && avatar.source ? avatar.source : null;

    return source ?
        <img src={`data:image/*;base64,${source}`} alt="avatar"/> :
        <img src={`https://via.placeholder.com/${size}`} alt="avatar"/>
};

export default Avatar;
