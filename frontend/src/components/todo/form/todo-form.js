import React from 'react';

import './todo-form.css';

import TextInput from "../../common/textinput/text-input";
import Button from "../../common/button/button";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheck, faPlus, faTimesCircle} from "@fortawesome/free-solid-svg-icons";

import {DAYS_OF_WEEK} from '../../../utils/constants';
import {FormattedMessage} from "react-intl";

const TodoForm = (props) => {

    const Days = () => {
        return DAYS_OF_WEEK.map(dayOfWeek => {
            const item = props.days.find(item => {
                if (item.day === dayOfWeek.day) return item;
            });

            const isEnabled = item ? item.enabled : false;

            return (
                <li key={dayOfWeek.day} className='todo-form__notification--item'>
                    <input type='checkbox'
                           name={dayOfWeek.day}
                           checked={isEnabled}
                           onChange={props.handleCheckboxChanges}/>
                    <label htmlFor={dayOfWeek.day}>
                        <FormattedMessage id={`todoform.day.${dayOfWeek.day}`} defaultMessage={dayOfWeek.day}/>
                    </label>
                    <div className="todo-form__icon--wrapper">
                        <FontAwesomeIcon icon={faPlus} className='todo-form__icon todo-form__icon--plus'/>
                        <FontAwesomeIcon icon={faCheck} className='todo-form__icon todo-form__icon--checked'/>
                    </div>
                </li>
            )
        });
    };

    const Completed = () => {
        return props.isCompleted ?
            <div className='todo-form__completed'>
                <FormattedMessage id='todoform.completed' defaultMessage='Completed'/>
            </div> : null
    };

    const ResultActionStatus = () => {
        const {actionInfo} = props;
        if (!actionInfo) return null;

        let msg = '';
        let classes = '';
        if (actionInfo.saveFailed) {
            classes = ' todo-form__result-status--failed';
            msg = 'Failed!';
        } else if (actionInfo.saveSuccess) {
            classes = ' todo-form__result-status--success';
            msg = 'Saved!';
        }

        if (!msg || !classes) return null;

        return (
            <div className={`todo-form__result-status ${classes}`}>
                {msg}
            </div>
        )
    };
    const nameLabel = <FormattedMessage id='todoform.input.label.name' defaultMessage='Name'/>;
    const descriptionLabel = <FormattedMessage id='todoform.input.label.description' defaultMessage='Description'/>;
    const btnLocal = <FormattedMessage id='todoform.btn.name' defaultMessage='Save'/>;
    return (
        <div className='todo-form'>
            <FontAwesomeIcon icon={faTimesCircle}
                             size='2x'
                             className='todo-form__close-icon'
                             onClick={props.closeWithoutSaving}/>
            <TextInput name='name'
                       cName='todo-form__text-input'
                       value={props.name}
                       label={nameLabel}
                       onChange={props.handleInputChanges}
                       autoComplete={false}/>
            <TextInput name='description'
                       cName='todo-form__text-input'
                       value={props.description}
                       label={descriptionLabel}
                       onChange={props.handleInputChanges}
                       autoComplete={false}/>
            <ul className='todo-form__notification'>
                <Days/>
            </ul>

            <div className='todo-form__scheduler'></div>

            <Completed/>
            <div className='todo-form__btn--wrapper'>
                <ResultActionStatus/>
                <Button name={btnLocal} onClick={props.saveOrUpdateTodo}/>
            </div>
        </div>
    )
};

export default TodoForm

