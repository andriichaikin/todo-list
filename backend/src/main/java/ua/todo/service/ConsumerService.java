package ua.todo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.todo.dao.ConsumerDao;
import ua.todo.dao.ConsumerSettingsDao;
import ua.todo.dto.ConsumerInfoDTO;
import ua.todo.pojo.Consumer;
import ua.todo.pojo.ConsumerSettings;

import java.util.Optional;

import static java.lang.String.format;

@Service
public class ConsumerService {

    @Autowired
    private ConsumerDao consumerDao;

    @Autowired
    private ConsumerSettingsDao consumerSettingsDao;

    public Optional<Consumer> findById(Long id) {
        return consumerDao.findById(id);
    }

    public void update(Consumer user) {
        consumerDao.update(user);
    }

    public void updateUserInfo(Consumer user, ConsumerInfoDTO info) {
        consumerDao.update(user
                .setName(info.getName())
                .setSurname(info.getSurname())
                .setEmail(info.getEmail())
                .setPhone(info.getPhone()));
    }

    public void setAvatar(Long id, String path) {

        Consumer user = consumerDao.findById(id)
                .orElseThrow(() ->
                        new IllegalStateException(format("Can not update user avatar. Consumer %s not found", id)));

        ConsumerSettings settings = user.getSettings();
        if (settings == null) {
            ConsumerSettings userSettings = new ConsumerSettings().setAvatarPath(path);
            consumerSettingsDao.saveOrUpdate(userSettings);
            consumerDao.update(user.setSettings(userSettings));
        } else {
            consumerSettingsDao.saveOrUpdate(settings.setAvatarPath(path));
        }
    }

    public void save(Consumer consumer) {
        consumerDao.save(consumer);
    }

    public void setLanguage(Consumer consumer, String lang) {
        if (consumer.getSettings() == null) {
            ConsumerSettings settings = new ConsumerSettings();
            consumer.setSettings(settings);
            consumerSettingsDao.saveOrUpdate(settings);
        }

        ConsumerSettings.Language selected = ConsumerSettings.Language.from(lang);
        if (selected == null) throw new IllegalStateException(
                format("Can not find language for %s value. User id %s", lang, consumer.getId()));

        consumer.getSettings().setLang(selected);

        consumerDao.update(consumer);
    }
}
