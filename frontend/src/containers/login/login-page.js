import React, {Component} from 'react';
import './login-page.css';

import TextInput from "../../components/common/textinput/text-input";
import Button from "../../components/common/button/button";

import {connect} from "react-redux";
import {authRequest, handleInputChanges} from "../../store/actions/auth-action";
import {Redirect} from "react-router-dom";
import {baseUrl} from "../../utils/API";
import ErrorBoundary from "../../hoc/error/error-boundary";
import {FormattedMessage} from "react-intl";

class LoginPage extends Component {

    state = {
        redirectToRegister: false,
    };

    handleInput = (e) => {
        const {name, value} = e.target;

        this.props.handleInputChanges(name, value);
    };

    login = () => {
        const {username, password} = this.props.auth;

        this.props.authRequest(username, password);
    };

    getErrorMsg = () => {
        let {hasError} = this.props.auth;

        return hasError ? <span className='logig__error-msg'>Invalid login or password!</span> : null;
    };

    getRedirect = () => {
        let {isAuthenticated} = this.props.auth;

        return isAuthenticated ? <Redirect to='/'/> : null;
    };

    goToRegistration = () => {
        this.setState({redirectToRegister: true});
    };

    getRedirectToRegistration = () => {
        if (this.state.redirectToRegister) return <Redirect to='/sign-up'/>

        return null;
    };



    handleError = () => {
        /*todo change cat img*/
        return (
            <div className='todos-error--wrapper'>
                     <span className='todos-error__text'>
                        Sorry, an exception occurred while login :(
                    </span>
                <img className='todos-error__img'
                     src={`${baseUrl}/images/cat_crying.png`}
                     alt="cat_crying"/>
            </div>
        )
    };

    render() {
        const {username, password} = this.props.auth;

        const Error = this.handleError;
        const ErrorMsg = this.getErrorMsg;
        const RedirectToMain = this.getRedirect;
        const RedirectToRegistration = this.getRedirectToRegistration;

        const loginLabel = <FormattedMessage id='login.form.login' defaultMessage='Login'/>;
        const passwordLabel = <FormattedMessage id='login.form.password' defaultMessage='Password'/>;
        const loginBtnName = <FormattedMessage id='login.form.btn.login.name' defaultMessage='Login'/>;
        const registrationBtnName = <FormattedMessage id='login.form.btn.registration.name' defaultMessage='Sign up'/>

        return (
            <ErrorBoundary customRender={<Error/>}>
                <div className="login">
                    <div className="login__form">
                        <ErrorMsg/>
                        <RedirectToMain/>
                        <RedirectToRegistration/>

                        <TextInput type='text'
                                   label={loginLabel}
                                   name='username'
                                   cName='login__input'
                                   value={username}
                                   onChange={this.handleInput}
                                   autoComplete={false}/>
                        <TextInput type='password'
                                   label={passwordLabel}
                                   name='password'
                                   cName='login__input'
                                   value={password}
                                   onChange={this.handleInput}
                                   autoComplete={false}/>

                        <div className='login_btns'>
                            <Button name={loginBtnName} onClick={this.login}/>
                            <Button name={registrationBtnName} onClick={this.goToRegistration}/>
                        </div>
                    </div>
                </div>
            </ErrorBoundary>
        )
    }
}

const mapStateToProps = state => ({
    auth: state.authReducer,
});

const mapDispatchToProps = dispatch => ({
    authRequest: (username, password) => dispatch(authRequest(username, password)),
    handleInputChanges: (property, value) => dispatch(handleInputChanges(property, value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
