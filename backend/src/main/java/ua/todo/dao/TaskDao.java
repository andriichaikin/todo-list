package ua.todo.dao;

import javax.transaction.Transactional;
import ua.todo.pojo.Task;
import ua.todo.pojo.Weekday;

import java.util.List;
import java.util.Optional;

@Transactional
public interface TaskDao {

    Optional<Task> findById(long id);

    Long findAllByConsumerCount(Long consumerId);

    List<Task> findAllByConsumerBatch(Long consumerId, int start, int amount);

    Long findAllByConsumerCount(Long consumerId, boolean isCompleted);

    List<Task> findAllByConsumerBatch(Long consumerId, int start, int amount, boolean isCompleted);

    Long save(Task task);

    void update(Task task);

    Long findAllByConsumerAndNameOrDescriptionCount(Long consumerId, String val);

    List<Task> findAllByConsumerAndNameOrDescription(Long consumerId, String val);

    Long findAllByConsumerAndDayCount(Long consumerId, Weekday.Day day);

    List<Task> findAllByConsumerAndDay(Long consumerId, Weekday.Day day, int start, int amount);
}
