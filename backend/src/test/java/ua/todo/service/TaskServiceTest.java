package ua.todo.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ua.todo.Application;
import ua.todo.pojo.Task;
import ua.todo.dao.TaskDao;
import ua.todo.pojo.CompareResult;
import ua.todo.util.TaskUtils;

import java.util.List;
import java.util.Optional;

import static java.lang.System.nanoTime;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class TaskServiceTest {

    private static final int AMOUNT = 10;
    private static final int START = 0;
    private static final long CONSUMER_ID = 0;

    @Autowired
    @InjectMocks
    private TaskService taskService;

    @Mock
    private TaskDao taskDao;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findByIdTest() {
        long indx = 0L;
        when(taskDao.findById(indx)).thenReturn(TaskUtils.getById(indx));

        assertThat(taskService.findById(indx)).isNotNull();
        verify(taskDao, times(1)).findById(indx);
    }

    @Test
    public void findTasksTest() {
        when(taskDao.findAllByConsumerBatch(CONSUMER_ID, START, AMOUNT)).thenReturn(TaskUtils.getTask(START, AMOUNT));

        List<Task> tasks = taskService.findAllByConsumerBatch(CONSUMER_ID, START, AMOUNT);

        assertThat(tasks).isNotEmpty();
        verify(taskDao, times(1)).findAllByConsumerBatch(CONSUMER_ID, START, AMOUNT);
    }

    @Test
    public void findTasksWithCompletedTest() {
        when(taskDao.findAllByConsumerBatch(CONSUMER_ID, START, AMOUNT, true))
                .thenReturn(TaskUtils.getTask(START, AMOUNT, true));

        List<Task> tasks = taskService.findAllByConsumerBatch(CONSUMER_ID, START, AMOUNT, true);

        assertThat(tasks).isNotEmpty();
        verify(taskDao, times(1)).findAllByConsumerBatch(CONSUMER_ID, START, AMOUNT, true);
    }

    @Test
    public void validateTaskTest() {
        Task task = new Task().setName("more then 3 chars");

        CompareResult cr = taskService.validateTask(task);
        assertThat(cr.isValid()).isTrue();
    }

    @Test
    public void notValidTaskTest() {
        Task task = new Task().setName("").setDescription("Name less then 3 char");

        CompareResult cr = taskService.validateTask(task);
        assertThat(cr.isValid()).isFalse();
    }

    @Test
    public void toggleCompletedTest() {
        Task task = new Task().setId(nanoTime()).setName("Toggled").setCompleted(false);

        when(taskDao.findById(task.getId())).thenReturn(Optional.of(task));
        doNothing().when(taskDao).update(task);

        taskService.toggleCompleted(task.getId());

        assertThat(task.isCompleted()).isTrue();
        verify(taskDao, times(1)).findById(task.getId());
        verify(taskDao, times(1)).update(task);


        taskService.toggleCompleted(task.getId());

        assertThat(task.isCompleted()).isFalse();
        verify(taskDao, times(2)).findById(task.getId());
        verify(taskDao, times(2)).update(task);
    }

    @Test
    public void setRemovedTest() {
        Task task = new Task().setId(0L).setName("test name");
        assert !task.isRemoved();

        when(taskDao.findById(task.getId())).thenReturn(Optional.of(task));
        doNothing().when(taskDao).update(task);

        taskService.setRemoved(task.getId());

        assertThat(task.isRemoved()).isTrue();
        verify(taskDao, times(1)).findById(task.getId());
        verify(taskDao, times(1)).update(task);
    }
}