package ua.todo.pojo;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "ConsumerSettings")
@Table(name = "consumer_settings")
public class ConsumerSettings extends BaseEntity {

    public static final String AVATAR_PATH = "avatar_path";
    public static final String LANGUAGE = "lang";

    @Column(name = AVATAR_PATH)
    private String avatarPath;

    @Column(name = LANGUAGE)
    @Enumerated(EnumType.STRING)
    @NotNull
    private Language lang = Language.ENGLISH;

    public enum Language {
        ENGLISH("en"), UKRAINIAN("ua");

        private String val;

        Language(String val) {
            this.val = val;
        }

        public String value() {
            return val;
        }

        public static Language from(String val) {
            for (Language lang : Language.values()) {
                if(lang.val.equalsIgnoreCase(val)) return lang;
            }
            return null;
        }
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public ConsumerSettings setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
        return this;
    }

    public Language getLang() {
        return lang;
    }

    public ConsumerSettings setLang(Language lang) {
        this.lang = lang;
        return this;
    }

    @Override
    public String toString() {
        return super.toString() +
                "ConsumerSettings{" +
                ", avatarPath='" + avatarPath + '\'' +
                ", lang='" + lang + '\'' +
                '}';
    }
}
