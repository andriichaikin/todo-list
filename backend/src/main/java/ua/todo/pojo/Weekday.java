package ua.todo.pojo;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "Weekday")
@Table(name = "weekday")
public class Weekday extends BaseEntity<Weekday> {

    public static final String DAY = "day";
    public static final String IS_ENABLED = "is_enabled";

    @Column(name = DAY)
    @Enumerated(EnumType.STRING)
    @NotNull
    private Day day;

    @Column(name = IS_ENABLED)
    private boolean isEnabled;

    @ManyToOne(optional = false)
    @JoinColumn(name = "fk_task")
    @JsonBackReference
    private Task task;

    public enum Day {
        SUNDAY("SUNDAY"), MONDAY("MONDAY"), TUESDAY("TUESDAY"), WEDNESDAY("WEDNESDAY"),
        THURSDAY("THURSDAY"), FRIDAY("FRIDAY"), SATURDAY("SATURDAY");

        private String val;

        Day(String val) {
            this.val = val;
        }

        public String value() {
            return val;
        }
    }

    public Day getDay() {
        return day;
    }

    public Weekday setDay(Day day) {
        this.day = day;
        return this;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public Weekday setEnabled(boolean enabled) {
        isEnabled = enabled;
        return this;
    }

    public Task getTask() {
        return task;
    }

    public Weekday setTask(Task task) {
        this.task = task;
        return this;
    }

    @Override
    public String toString() {
        return super.toString() +
                "Weekday{" +
                "day=" + day +
                ", isEnabled=" + isEnabled +
                '}';
    }
}
