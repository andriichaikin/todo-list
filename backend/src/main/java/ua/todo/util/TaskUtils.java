package ua.todo.util;

import org.springframework.stereotype.Component;
import ua.todo.pojo.Task;
import ua.todo.pojo.Weekday;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.EMPTY_LIST;
import static java.util.stream.Collectors.toList;
import static org.springframework.util.StringUtils.isEmpty;
import static ua.todo.pojo.Weekday.Day.*;

@Component
public class TaskUtils {

    private static List<Task> defaultTask = new LinkedList<>();

    public static Optional<Task> getById(long id) {
        return defaultTask
                .stream()
                .filter(task -> task.getId().equals(id))
                .findFirst();
    }

    public static void add(Task task) {
        defaultTask.add(task);
    }

    public static List<Task> getTask(int start, int amount) {
        return getTask(defaultTask, start, amount);
    }

    private static List<Task> getTask(List<Task> list, int start, int amount) {
        int size = list.size();
        if (size < start) return EMPTY_LIST;

        int lastIndx = start + amount;
        if (size < lastIndx) lastIndx = size;

        return list.subList(start, lastIndx);
    }

    public static List<Task> getTask(int start, int amount, boolean isCompleted) {
        return getTask(defaultTask, start, amount, isCompleted);
    }

    private static List<Task> getTask(List<Task> list, int start, int amount, boolean isCompleted) {
        List<Task> result = new LinkedList<>();

        for (Task task : list) {
            if (task.isCompleted() == isCompleted) result.add(task);
        }

        int toIndex = start + amount;
        if (result.size() < toIndex) toIndex = result.size();

        return result.subList(start, toIndex);
    }

    public static Long getLastId() {
        return defaultTask.stream()
                .max(Comparator.comparing(Task::getId))
                .get().getId();
    }

    public static long findAllCount() {
        return defaultTask.size();
    }

    public static long findAllCount(boolean isCompleted) {
        return defaultTask.stream()
                .filter(task -> task.isCompleted() == isCompleted)
                .count();
    }

    public static List<Task> findAllByNameOrDescription(String matcher) {

        String val = matcher == null ? "" : matcher.toLowerCase();
        List<Task> found = defaultTask.stream()
                .filter(task -> task.getName().toLowerCase().contains(val) ||
                        (!isEmpty(task.getDescription()) && task.getDescription().toLowerCase().contains(val)))
                .collect(toList());

        return found;
    }

    public static List<Task> findAllByDay(String day, int start, int amount) {
        List<Task> found = defaultTask.stream()
                .filter(task -> task.isDayEnabled(day))
                .collect(toList());

        return found;
    }

    static {
        for (long indx = 0; indx < 60; indx++) {
            Task task = new Task()
                    .setId(indx)
                    .setName("name " + indx)
                    .setDescription("description " + indx);

            setDays(task);

            if (indx < 3) {
                task.setDayEnabled(MONDAY.value(), true);
                task.setDayEnabled(TUESDAY.value(), true);
                task.setDayEnabled(WEDNESDAY.value(), true);
                task.setDayEnabled(THURSDAY.value(), true);
                task.setDayEnabled(FRIDAY.value(), true);
                task.setDayEnabled(SATURDAY.value(), true);
                task.setDayEnabled(SUNDAY.value(), true);
            }

            if (indx > 7 && indx < 25) {
                task.setDayEnabled(MONDAY.value(), true);
                task.setDayEnabled(TUESDAY.value(), true);
                task.setDayEnabled(WEDNESDAY.value(), true);
                task.setDayEnabled(THURSDAY.value(), true);
                task.setDayEnabled(FRIDAY.value(), true);
            }

            if (indx > 30 && indx < 35) {
                task.setDayEnabled(SATURDAY.value(), true);
                task.setDayEnabled(SUNDAY.value(), true);
            }


            if (indx > 40) task.setCompleted(true);
            else task.setCompleted(false);

            defaultTask.add(task);
        }
    }

    private static void setDays(Task task) {
        task.getDays().add(new Weekday().setDay(MONDAY));
        task.getDays().add(new Weekday().setDay(TUESDAY));
        task.getDays().add(new Weekday().setDay(WEDNESDAY));
        task.getDays().add(new Weekday().setDay(THURSDAY));
        task.getDays().add(new Weekday().setDay(FRIDAY));
        task.getDays().add(new Weekday().setDay(SATURDAY));
        task.getDays().add(new Weekday().setDay(SUNDAY));
    }
}
