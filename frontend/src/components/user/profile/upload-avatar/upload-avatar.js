import React, {Fragment} from 'react';

import './upload-avatar.css';

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faImages, faTimesCircle} from "@fortawesome/free-solid-svg-icons";
import {FormattedMessage} from "react-intl";

const UploadAvatar = (props) => {

    const {closeModal, handleImage, uploadImgOnServer} = props;

    return (
        <Fragment>
                <div className='upload_avatar'>
                <FontAwesomeIcon icon={faTimesCircle}
                                 size='2x'
                                 className='upload_avatar__close-icon'
                                 onClick={closeModal}/>
                <div className='upload_avatar__title'>
                    <span>
                        <FormattedMessage id='profile.avatar.upload.caption' defaultMessage='Select profile photo2'/>
                    </span>
                </div>
                <input type='file' onChange={handleImage}/>
                <button onClick={uploadImgOnServer}>
                    <FormattedMessage id='profile.avatar.upload.btn.name' defaultMessage='Upload'/>
                </button>
                <div className='upload_avatar__drag-and-drop'>
                    <div className='upload_avatar__bg'>
                        <span className='upload-avatar__bg--title'>
                            <FormattedMessage id='profile.avatar.upload.drag-n-drop.caption' defaultMessage='Drag and drop'/>
                        </span>
                        <FontAwesomeIcon icon={faImages} size='10x'/>
                    </div>
                </div>
            </div>
        </Fragment>
    )
};

export default UploadAvatar;
