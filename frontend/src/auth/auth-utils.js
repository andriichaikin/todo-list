const JWT_TOKEN = 'jwtToken';

export const getToken = () => {
    return localStorage.getItem(JWT_TOKEN);
};

export const saveToken = (token) => {
    localStorage.setItem(JWT_TOKEN, token);
};

export const removeToken = () => {
    localStorage.removeItem(JWT_TOKEN);
};

export const isTokenExist = () => {
    return localStorage.getItem(JWT_TOKEN) !== null &&
        localStorage.getItem(JWT_TOKEN) !== undefined;
};

export const decodeLocaleStorageToken = () => {
    const token = getToken();
    if (!token) return;

    return decodeToken(token);
};

export const decodeToken = (token) => {
    const jwt = require('jsonwebtoken');
    const tkn = token.substring(7, token.length);

    return  jwt.decode(tkn);
};

export const isTokenValid = (token) => {
    if (!token) return false;

    if (!__checkAuthenticated(token)) return false;

    const diff = getDiffWithNow(token.exp);
    return token && diff > 0;
};

export const isExpiredSoon = () => {
    let decoded = decodeLocaleStorageToken();
    if (!decoded) return false;

    let _5_minutes = 300 * 1000;
    const diff = getDiffWithNow(decoded.exp);

    return 0 < diff && diff < _5_minutes;
};

function getDiffWithNow(expired) {
    let time = new Date().getTime();
    let exp = expired * 1000;
    return exp - time;
}
const __checkAuthenticated = (token) => {
    return token && Number.isInteger(token.id) && token.id > -1;
};


