import {TOGGLE_SPINNER} from "../constants";

export const toggleSpinner = (bool) => {
    const action = {
        type: TOGGLE_SPINNER,
        isLoading: bool,
    };
    return action;
};
