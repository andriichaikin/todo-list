import React, {Component} from 'react';

import './upload-avatar-container.css';
import UploadAvatar from "../../../../components/user/profile/upload-avatar/upload-avatar";
import {connect} from "react-redux";

import {attachFile, rmAttachedFile, uploadFileAction} from '../../../../store/actions/user/user-avatar-action';
import * as ReactDom from "react-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faImages, faTimesCircle} from "@fortawesome/free-solid-svg-icons";
import Button from "../../../../components/common/button/button";
import {faTrash} from "@fortawesome/free-solid-svg-icons/faTrash";
import {baseUrl} from "../../../../utils/API";
import ErrorBoundary from "../../../../hoc/error/error-boundary";
import {FormattedMessage} from "react-intl";

class UploadAvatarContainer extends Component {

    state = {
        dragging: false,
    };

    dropRef = React.createRef();

    handleImage = (e) => {
        if (!e.target.files) {
            this.attachSelectedFile(undefined);
        } else if (e.target.files.length > 1) {
            this.attachSelectedFile(undefined);
        } else {
            this.attachSelectedFile(e.target.files[0]);
        }

    };

    attachSelectedFile = (file) => {
        this.props.attachFile(file);
    };

    rmSelectedFile = () => {
        this.props.rmAttachedFile();
    };

    uploadImgOnServer = () => {
        const {file} = this.props;
        if (!file) {
            console.log('No file for uploading');
            return;
        }

        const {id} = this.props.auth;

        const formData = new FormData();
        formData.append('file', file);
        console.log(formData);

        this.props.uploadFileAction(id, file);
    };

    componentDidMount() {
        let node = ReactDom.findDOMNode(this.dropRef);
        if (node) {
            node.addEventListener('dragenter', this.handleDragIn);
            node.addEventListener('dragleave', this.handleDragOut);
            node.addEventListener('dragover', this.handleDragOver);
            node.addEventListener('drop', this.handleDrop);
        }
    }

    componentWillUnmount() {
        const node = ReactDom.findDOMNode(this.dropRef);
        if (node) {
            node.removeEventListener('dragenter', this.handleDragIn);
            node.removeEventListener('dragleave', this.handleDragOut);
            node.removeEventListener('dragover', this.handleDragOver);
            node.removeEventListener('drop', this.handleDrop);
        }
    }

    handleDragIn = (e) => {
        e.preventDefault();
        e.stopPropagation();
        console.log('handleDragIn: ' + e + ' , dragging: ' + this.state.dragging);
        console.log(this.state.dragCounter);

        this.setState((prevState, props) => {
            return {
                ...prevState,
                dragCounter: this.state.dragCounter++,
            };
        });

        if (e.dataTransfer.items && e.dataTransfer.items.length > 0) {
            this.setState({dragging: true})
        }

        console.log(this.state.file);
    };

    handleDragOut = (e) => {
        e.preventDefault();
        e.stopPropagation();
        console.log('handleDragOut: ' + e + ' , dragging: ' + this.state.dragging);
        console.log(this.state.dragCounter);

        this.setState((prevState, props) => {
            return {
                ...prevState,
                dragCounter: this.state.dragCounter--
            };
        });

        if (this.state.dragCounter > 0) return;

        this.setState({dragging: false})
    };

    handleDragOver = (e) => {
        e.preventDefault();
        e.stopPropagation();
        console.log('handleDragOver: ' + e + ' , dragging: ' + this.state.dragging);

        this.setState({dragging: false});
        if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
            this.attachSelectedFile(e.dataTransfer.files[0]);
        }
    };

    handleDrop = (e) => {
        e.preventDefault();
        e.stopPropagation();
        console.log('handleDrop: ' + e + ' , dragging: ' + this.state.dragging);

        this.setState({dragging: false});
        if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
            this.attachSelectedFile(e.dataTransfer.files[0]);
            this.setState({dragCounter: 0});
        }
    };

    handleError = () => {
        /*todo change cat img*/
        return (
            <div className='todos-error--wrapper'>
                     <span className='todos-error__text'>
                        Sorry, an exception occurred while uploading avatar :(
                    </span>
                <img className='todos-error__img'
                     src={`${baseUrl}/images/cat_crying.png`}
                     alt="cat_crying"/>
            </div>
        )
    };

    render() {
        const Error = this.handleError;
        const btnName = <FormattedMessage id='profile.avatar.upload.btn.name' defaultMessage='Upload'/>;


        return (
            <ErrorBoundary customRender={<Error/>}>
                <div className="upload-avatar">
                    <div className='upload-avatar'>
                        <FontAwesomeIcon icon={faTimesCircle}
                                         size='2x'
                                         className='upload-avatar__close-icon'
                                         onClick={this.props.closeModal}/>
                        <div className='upload-avatar__title'>
                            <span>
                                <FormattedMessage id='profile.avatar.upload.caption'
                                                  defaultMessage='Select profile photo'/>
                            </span>
                        </div>

                        <div className='upload-avatar__drag-and-drop' ref={node => this.dropRef = node}>
                            {this.props.file ?
                                <div className='upload-avatar__file-name'>
                                    {this.props.file.name}
                                    <FontAwesomeIcon icon={faTrash}
                                                     className='upload-avatar__file-name--delete'
                                                     onClick={this.rmSelectedFile}/>
                                </div> : null
                            }

                            <span className='upload-avatar__bg--title'>
                                <FormattedMessage id='profile.avatar.upload.drag-n-drop.caption'
                                                  defaultMessage='Drag and drop'/>
                            </span>
                            <label htmlFor='input-avatar_id'>
                                <FontAwesomeIcon icon={faImages}
                                                 size='10x'
                                                 className='upload-avatar__select-icon-btn'/>
                            </label>
                            <input id='input-avatar_id'
                                   type='file'
                                   className='upload-avatar__input'
                                   onChange={this.handleImage}/>
                            {this.props.file ? <Button name={btnName}
                                                       cName='upload-avatar__upload-btn'
                                                       onClick={this.uploadImgOnServer}/> : null
                            }
                        </div>
                    </div>
                </div>
            </ErrorBoundary>
        )
    }
}

const mapStateToProps = state => ({
    file: state.userAvatarReducer.file,
    isLoading: state.userAvatarReducer.isLoading,
    auth: state.authReducer,
});

const mapDispatchToProps = dispatch => ({
    attachFile: (file) => dispatch(attachFile(file)),
    rmAttachedFile: () => dispatch(rmAttachedFile()),
    uploadFileAction: (consumerId, file, isLoading) => dispatch(uploadFileAction(consumerId, file, isLoading)),

});

export default connect(mapStateToProps, mapDispatchToProps)(UploadAvatarContainer);
